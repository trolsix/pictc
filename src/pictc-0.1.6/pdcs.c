/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018-2020 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------*/
/*                                               */
/*    read _lisdfun.txt                          */
/*                                               */
/*-----------------------------------------------*/

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "ptcutil.h"
#include "list.h"

/*-----------------------------------------------*/

#define STEPMODE 0

/*-----------------------------------------------*/

#define GETNUMB(WHERE,POINT) getnumnwathever ( &WHERE, POINT, sizeof(WHERE) );
#define MALLOCRET(IT,POINT) POINT=malloc(IT*sizeof(*(POINT))); if((POINT)==NULL) return 1;

/*-----------------------------------------------*/

extern uint8_t debufcount;
extern char *fnmap;

/*-----------------------------------------------*/

extern uint32_t value;
extern uint16_t resram, resstack, ressum;
extern int16_t numberdec, numberdecmax, numberdecmaxall, numberdecloc;
extern uint8_t valw, rstckhard;

static char *linetmp;
static size_t linesiz = 1024;

extern char asmarg1[32];
extern char asmarg2[32];
extern char asmarg3[32];
extern char asmarg4[32];
extern char afterupper[32];

struct fun {
	char name[32];
	char what;
	int16_t stack;
	uint8_t regist;
};

/*-----------------------------------------------*/

FILE * fileldf;
FILE * filem;

uint16_t funnumb, itemsnumb, labnumb;
struct fun * funwsk, *faw;
	
/*-----------------------------------------------*/

static uint8_t gfopen(void);
static void * readdata ( void );
void readitem ( void );
int32_t getnrlabel( uint32_t );
void flowback ( void );
void backtojump ( void );
void printsummary ( void );

/*-----------------------------------------------*/

int32_t getnrlabel( uint32_t l ) {
	
	uint32_t i;
	char * n;
	struct fun * f;
	
	f = funwsk;
	n = f[l].name;
	
	for( i=0; i<itemsnumb; ++i ) {
		if(('L'==f->what)||('l'==f->what)||('F'==f->what))
			if ( 0 == strcmp( f->name, n ) )
				return i;
		++f;
	}
	
	/* should never happen */
	if(debufcount)
		fprintf( stderr, "don't find label on the list\n" );
	
	exit(1);
}

/*-----------------------------------------------*/

uint8_t isonthelist ( uint32_t *list, uint32_t what, uint32_t max );
void zerval(void);

/*-----------------------------------------------*/

uint32_t *listpath, *listjump, *listlab, *listcall;
uint32_t nrpath, nrfcall;

uint32_t nrwork, nrljump, nrlab;
uint8_t nrloop;
uint8_t checkalljump;

/*-----------------------------------------------*/

struct funsumary {
	uint32_t nronfaw;		/* nr on the list */
	uint32_t adr;			/* adres in flash */
	int16_t stckrfun;		/* for return funkcion */
	int16_t stckxfun;		/* for return funkcion */
	int16_t stckxall;		/* max sum */
	uint8_t stckhard;		/* sum of call */
	uint8_t done;		   /* was done */
	uint8_t registf;		/* reg for fun  */
	uint8_t regista;		/* max reg */
};

struct funsumary * funsum;

/*-----------------------------------------------*/

uint32_t findnrfun ( uint32_t nr ) {
	
	uint16_t i;
	struct funsumary * fs;
	
	for ( fs=funsum, i=0; i<funnumb; ++i, ++fs )
		if( fs->nronfaw == nr ) return i;
	
	return 0x80000000;
}

/*-----------------------------------------------*/

uint32_t findadrfun ( uint32_t adr ) {
	
	struct funsumary * fs;
	uint16_t i;
	
	fs = funsum;
	
	for ( i=0; i<funnumb; ++i, ++fs ) {
		if( fs->adr == adr ) return i;
	}
	
	return 0x80000000;
}

/*-----------------------------------------------*/

uint32_t getnrinlist ( uint32_t *list, uint32_t what, uint32_t max ) {
	
	while(max--) {
		if( what == list[max] ) return max;
	}

	return 0;
}

/*-----------------------------------------------*/
/*

1. find all function and read max register
calculate:
- soft stack usage for fun
- max soft stack usage for fun
- max soft stack usage for fun with call
- max hard stack usage for fun with call

- calculate max for not return funkcion

- if read map files print original name

*/

uint8_t dcs ( void ) {
	
	uint32_t ms;
	
	debufcount = 2;
	
	if( gfopen() ) return 1;
	
	if(debufcount>1) fprintf( stderr, "dcs start\n" );
	
	linetmp = malloc(linesiz);
	if( linetmp == NULL ) return 1;
	
	checkalljump = 0;
	
	/* count number items */
	readitem ();
	
	MALLOCRET(itemsnumb,funwsk);
	MALLOCRET(itemsnumb,listpath);
	MALLOCRET(labnumb,listjump);
	MALLOCRET(labnumb,listlab);
	MALLOCRET(funnumb,funsum);
	MALLOCRET(funnumb,listcall);

	/* read data */
	fseek( fileldf, 0, SEEK_SET );
	funwsk = readdata();
	if( NULL == funwsk ) return 2;

	/* mem summary */

	ms = itemsnumb * sizeof(*funwsk);
	ms += itemsnumb*sizeof(*listpath);
	ms += labnumb*sizeof(*listjump);
	ms += labnumb*sizeof(*listlab);
	ms += funnumb*sizeof(*funsum);
	
	if(debufcount>1)
		fprintf( stderr, "alloc %u bytes\n" , ms );
	
	nrloop = 1;

	LOOP_SUM:

	fseek( fileldf, 0, SEEK_SET );
	
	nrlab = 0;
	nrljump = 0;
	nrpath = 0;
	nrwork = 0;
	nrfcall = 0;

	for ( ; nrwork<itemsnumb; ) {
		
		if(nrwork>=itemsnumb) return 1;
		faw = funwsk + nrwork;

		if(debufcount>2)
			fprintf( stderr, "%4u work %4u lab %3u %c %3i %s" ,
				nrpath, nrwork, nrlab, faw->what, faw->stack, faw->name );

		if ( ( nrpath == 0  ) && ( faw->what != 'F' ) ) {
			++nrwork;
			if(debufcount>2)
				fprintf( stderr, "\n" );
			continue;
		}

		/* find new */
		if( faw->what=='F') {
			uint32_t fn;
			
			if((nrloop == 0)&&(debufcount>2)) {
				fprintf( stderr, "-----------------------------\n" );
				fprintf( stderr, "fun %u work: %u" , nrpath, nrwork );
				fprintf( stderr, " %c %s\n" , faw->what, faw->name );
			}
			/* if was maked skip */
			fn = findnrfun(nrwork);
			if ( funsum[fn].done & 0x01 ) {
				++nrwork;
				continue;
			}
			
			listpath[nrpath++] = nrwork++;
			if(debufcount>2)
				fprintf( stderr, "\n" );
			continue;
		}
		
		/* find goto or branch */
		if( ( faw->what=='g') || ( faw->what=='b') ){
			
			int32_t nritem = getnrlabel( nrwork );
			
			listpath[nrpath] = nrwork;
			
			if( !checkalljump ){
				if ( isonthelist( listlab, nritem, nrlab ) ) {
					++nrwork;
					++nrpath;
					if(debufcount>2)
						fprintf( stderr, " is in listlab\n" );
					continue;
				}
			}

			if( isonthelist ( listpath, nritem, nrpath ) ) {
				++nrwork;
				if(debufcount>2)
					fprintf( stderr, " is in list\n" );
			} else {
				listjump[nrljump++] = nrpath;
				nrwork = nritem;
				if(debufcount>2)
					fprintf( stderr, " jump %u\n", nrwork );
			}
			++nrpath;
			continue;
		}
		
		/* find label */
		if( ( faw->what=='l') || ( faw->what=='L') ) {
			
			if ( !isonthelist( listlab, nrwork, nrlab ) )
				listlab[nrlab++] = nrwork;
			listpath[nrpath++] = nrwork++;
			
			if(debufcount>2) fprintf( stderr, "\n" );
			continue;
		}
		
		if( faw->what=='c') {
			uint32_t nritem, i;
			
			/* skip in first look */
			listpath[nrpath] = nrwork;
			
			if( nrloop == 0 ) {
				++nrwork;
			} else {
				nritem = getnrlabel( nrwork );
				
				/* if exist means recursive and warn and calculate */
				/* or get the next element */
				if( isonthelist ( listpath, nritem, nrpath ) ) {
					if(debufcount>1)
						fprintf( stderr, "recursive %s\n", faw->name );
					flowback ();
					continue;
				}
				/* if funkcion was maked just skip */
				i = findnrfun ( nritem );
				
				/* if get stack calculate skip this call */
				if ( -0x8000 != funsum[i].stckrfun ){
					++nrwork;
					++nrpath;
					if(debufcount>2)
						fprintf( stderr, " skip call\n" );
					continue;
				}
				
				if(debufcount>2)
					fprintf( stderr, " jump %u %u", nritem, nrpath );
				listjump[nrljump++] = nrpath;
				nrwork = nritem;
				listcall[nrfcall++] = nrwork;
			}
			
			++nrpath;
			
			if(debufcount>2)
				fprintf( stderr, " j: %3u c: %3u\n", nrljump, nrfcall );
			continue;
		}
		
		/* find linfo */
		if ( ( faw->what=='i') || ( faw->what=='p') ){
			listpath[nrpath++] = nrwork++;
			if(debufcount>2)
				fprintf( stderr, "\n" );
			continue;
		}
		
		/* find end */
		if( faw->what=='r') {
			listpath[nrpath++] = nrwork++;
			if(debufcount>2)
				fprintf( stderr, "\n" );
			flowback ();
			continue;
		}
		
		
		/* find end */
		if( faw->what=='e') {
			listpath[nrpath++] = nrwork++;
			if(debufcount>2)
				fprintf( stderr, "\n" );
			flowback ();
			continue;
		}
		
		nrwork++;
		fprintf( stderr, "=\n" );
	}

	if ( ++nrloop < 2 ) {
		if(debufcount>2) {
			fprintf( stderr, "============ loop2 ==========\n" );
		}
		goto LOOP_SUM;
	}
	
	printsummary();
	
	if ( fileldf ) fclose(fileldf);
	if ( filem ) fclose(filem);
	
	return 0;
}

/*-----------------------------------------------*/

char *mname;
uint32_t madr;

uint8_t getentrycode ( void ) {
	
	char * wsl;
	
	MALLOCRET(128,mname);
	
	while ( 0 < getline ( &linetmp , &linesiz, filem ) ) {
		
		wsl = getnwathever ( mname, linetmp, 128 );
		wsl = getnwathever ( asmarg1, wsl, 32 );
		wsl = GETNUMB( madr, wsl );
		wsl = getnwathever ( asmarg2, wsl, 32 );
		
		if ( 0 != strcmp( "code", asmarg1 ) ) continue;
		if ( 0 != strcmp( "program", asmarg2 ) ) continue;
		
		/*
		fprintf( stderr, "f %s a 0x%06x\n", mname, madr );
		*/
		
		return 0;
	}
	
	
	return 1;
}

/*-----------------------------------------------*/
/*
	uint32_t nronfaw;
	uint32_t adr;
	int16_t stckrfun;
	int16_t stckxfun;
	int16_t stckxall;
	uint8_t stckhard;
	uint8_t registf;
	uint8_t regista;
*/

/*

S_file__fun       code   0x002a64    program   0x000154
_fun   0x002a64    program     static file.asm

*/

void printsummary ( void ) {
	
	uint32_t i, nrf;
	struct fun * f;
	struct funsumary *fs;
	
	fprintf( stdout, "\n" );
	fprintf( stdout, " %40s", "name" );
	fprintf( stdout, "      adr" );
	fprintf( stdout, "  stckf" );
	fprintf( stdout, " stcka" );
	fprintf( stdout, " stckh" );
	
	if ( filem ) {
		
		while ( 0 == getentrycode() ){
			/* find fun */
			nrf = findadrfun ( madr );
			if ( nrf >= funnumb ) continue;
			
			fs = funsum + nrf;
			
			fprintf( stdout, "\n" );
			fprintf( stdout, " %40s", mname );
			fprintf( stdout, " 0x%06x", fs->adr );
			fprintf( stdout, " %5u", fs->stckxfun );
			fprintf( stdout, " %5u", fs->stckxall );
			fprintf( stdout, " %5u", fs->stckhard );
			
			/* deactivate adr */
			fs->adr = 0x80000000;
			free(mname);
		}
		
	}
	
	for ( i=0, fs=funsum; i<funnumb; ++i, ++fs ) {
			
		f = funwsk + fs->nronfaw;
		if( 0x80000000&fs->adr) continue;
		fprintf( stdout, "\n" );
		fprintf( stdout, " %40s", f->name );
		fprintf( stdout, " 0x%06x", fs->adr );
		fprintf( stdout, " %5u", fs->stckxfun );
		fprintf( stdout, " %5u", fs->stckxall );
		fprintf( stdout, " %5u", fs->stckhard );
	}
	
	fprintf( stdout, "\n" );
}

/*-----------------------------------------------*/
/* analyze from 0 -> nrwork */

void flowback ( void ) {

	struct fun * f;
	uint32_t elem, nrtw, belem;
	int16_t stmp1, stmp2;
	uint8_t numberhardstc;
	
	if(debufcount>3)
		fprintf( stderr, "flow back welcome" );
	
	/* find call funkcion on the list work */
	
	if( nrloop == 1 ) {
		
		/* if has no call means top funkcion */
		if( nrfcall == 0 ) {
			if(debufcount>3)
				fprintf( stderr, " nrfcall 0 top fun" );
			
			elem = listpath[0];
			belem = findnrfun (elem);
			if(debufcount>3)
				fprintf( stderr, " s: %u %u %s", listpath[0], elem, funwsk[elem].name );
			elem = 0;
			/* if funkcion was calulated skip this */
		} else {
			elem = listcall[nrfcall-1];
			if(debufcount>3)
				fprintf( stderr, " s: %u %s", elem, funwsk[elem].name );
			/* belem = findnrfun ( getnrlabel(elem) ); */
			belem = findnrfun ( elem );
			elem = getnrinlist ( listpath, elem, nrpath );
		}
		
		if(debufcount>3)
			fprintf( stderr, " w: %u f: %u %i\n", elem, belem, (int32_t)funsum[belem].stckxfun );
		
		numberdec = 0;
		numberdecmax = funsum[belem].stckxfun;
		numberhardstc = funsum[belem].stckhard;
		
		if(nrpath==0) exit(1);

		nrtw = listpath[elem];
		f = funwsk + nrtw;
		
		/* analyze fun branch but if 'r' dont update stckr */
		while( ++elem < nrpath ) {
			uint32_t fc;
			int16_t st;
			
			st = 0;
			nrtw = listpath[elem];
			f = funwsk + nrtw;
			numberdec += f->stack;
			
			if ( numberdecmax < numberdec ) numberdecmax = numberdec;
			if ( funsum[belem].stckxall < numberdecmax )
					funsum[belem].stckxall = numberdecmax;
			
			if(debufcount>3){
				fprintf( stderr, " %c", f->what );
				fprintf( stderr, " %3u.%3u.%3i", elem, nrtw, numberdec );
				fprintf( stderr, " %s", f->name );
			}
			
			/* stack for push */
			
			if( ( f->what == 'p' ) && ( 0 == numberhardstc ) )
				numberhardstc = 1;
			
			if( f->what == 'c' ) { /* read stack */
				
				fc = findnrfun ( getnrlabel( nrtw ) );
				
				/* hardware stack */
				if( (funsum[fc].stckhard+1) > numberhardstc ) {
					numberhardstc = funsum[fc].stckhard + 1;
				}
				if(debufcount>3)
					fprintf( stderr, " hs %3u", funsum[fc].stckhard+1 );
				
				/* software stack */
				st = funsum[fc].stckxall;
				if(debufcount>3)
					fprintf( stderr, " sa %3i", st  );
				
				if( funsum[belem].stckxall<(numberdec+st) )
					funsum[belem].stckxall = numberdec+st;
				
				st = funsum[fc].stckrfun;
				if(debufcount>3)
					fprintf( stderr, " sr %3i", st  );
				
				if ( (int16_t) 0x8000 != st ) { /* new */
					numberdec += st;
				} else {
					if(debufcount>3)
						fprintf( stderr, " fun not return" );
				}
				if ( numberdecmax < numberdec ) numberdecmax = numberdec;
				if ( funsum[belem].stckxall < numberdecmax )
					funsum[belem].stckxall = numberdecmax;
			}
			
			if(debufcount>3)
				fprintf( stderr, "\n" );
		}
		
		/* max witch call inside stckxall */
		funsum[belem].stckxfun = numberdecmax;
		funsum[belem].stckhard = numberhardstc;
		
		/* compare and write if has r */
		if ( f->what == 'r' ) {
			if ( (int16_t)0x8000 == funsum[belem].stckrfun ) { /* new */
				
				funsum[belem].stckrfun = numberdec;
				
			} else { /* compare */
				
				if( funsum[belem].stckrfun < 0 )
					stmp1 = -funsum[belem].stckrfun;
				else stmp1 = funsum[belem].stckrfun;
				
				if( numberdec < 0 ) stmp2 = -numberdec;
				else stmp2 = numberdec;
				
				if( stmp2 != stmp1 ) {
					if(debufcount>0) {
						uint32_t nrf = funsum[belem].nronfaw;
						fprintf( stderr, " %s", funwsk[nrf].name );
						fprintf( stderr, " differ stack %i %i", funsum[belem].stckrfun, numberdec );
					}
					if( stmp2 > stmp1 ) {
						if(debufcount>0)
							fprintf( stderr, " get: %i", numberdec );
						funsum[belem].stckrfun = numberdec;
					}
					if(debufcount>0)
						fprintf( stderr, "\n");
				}
			}
		}
		
		if(debufcount>3)
			fprintf( stderr, " stack" );
		
		if ( (int16_t)0x8000 == funsum[belem].stckrfun ) {
			if(debufcount>3)
				fprintf( stderr, " r --- " );
		} else {
			if(debufcount>3)
				fprintf( stderr, " r %3i ", funsum[belem].stckrfun );
		}
		if(debufcount>3){
			fprintf( stderr, " x %3i ", funsum[belem].stckxfun );
			fprintf( stderr, " a %3i ", funsum[belem].stckxall );
			fprintf( stderr, " h %3i ", funsum[belem].stckhard );
		}
		
		
		/* return to the point jump */
		if ( nrljump == 0 ) {
			funsum[belem].done |= 0x01;
			if(debufcount>3) {
				fprintf( stderr, " end: %s", funwsk[listpath[0]].name );
				fprintf( stderr, " ----\n");
			}
			nrpath = 0;
			nrlab = 0;
			zerval();
			nrwork = listpath[nrpath];
			++nrwork;
			#if STEPMODE==1
			fgetc(stdin);
			#endif
			return;
		}
		
		elem = listjump[--nrljump];
		
		if(debufcount>3)
			fprintf( stderr, " elem jump %3u", elem );
		
		while(1) {
			--nrpath;
			if(nrpath == 0 ) {
				if(debufcount>0)
					fprintf( stderr, "Bad nrpath = 0\n" );
				exit(1);
			}
			if( elem == nrpath ) {
				nrwork = listpath[nrpath];
				/*
				fprintf( stderr, "work %u", nrwork );
				*/
				/* if jump is call remove call for listcall */
				f = funwsk + nrwork;
				if( f->what == 'c' ) {
					funsum[belem].done |= 0x01;
					if(debufcount>3)
						fprintf( stderr, " jump call" );
					if(nrfcall)--nrfcall; /* bad if this is 0 */
					if(debufcount>3)
						fprintf( stderr, " %3u", listcall[nrfcall] );
				}
				if ( nrljump ) {
					++nrwork;
					++nrpath;
					break;
				}
				break;
			}
		}
		
		if(debufcount>3)
			fprintf( stderr, " flow back byby %u %u\n", nrwork, nrpath );

		return;
	}
	

	/* only for read register - not work yet  */
	/* beck until elemn is on the jump list  */

	if ( nrljump == 0 ) elem = -1;
	else elem = listjump[--nrljump];
	
	if(debufcount>2)
		fprintf( stderr, " b elem %i \n" , elem );
	
	while(1){
		--nrpath;

		f = funwsk + listpath[nrpath];
		if ( f->regist > resram ) resram = f->regist;
		
		if( f->what == 'r' ) numberdec = 0;
		
		numberdec += f->stack;
		fprintf( stderr, "d %4u stck %3i %3i\n", nrpath, f->stack, numberdec );
		
		if(nrpath == 0 ) {
			fprintf( stderr, "regist: %u\n" , resram );
			fprintf( stderr, "stacks: %u\n" , numberdecmax );
			if(debufcount>2) {
				fprintf( stderr, "nrpath equal 0\n" );
				fprintf( stderr, "-----------------------------\n" );
			}
			nrlab = 0;
			zerval();
			nrwork = listpath[nrpath];
			++nrwork;
			return;
		}
		
		if( elem == nrpath ) {
			nrwork = listpath[nrpath];
			if ( nrljump ) {
				++nrwork;
				++nrpath;
				break;
			}

		}
	}
	
	if(debufcount>2)
		fprintf( stderr, "flow back byby %u %u\n", nrwork, nrpath );
}


/*-----------------------------------------------*/

void readitem ( void ) {

	funnumb = 0;
	itemsnumb = 0;
	labnumb = 0;
	
	while ( 0 < getline ( &linetmp , &linesiz, fileldf ) ) {
		++itemsnumb;
		if( linetmp[0] == 'F') ++funnumb;
		if( ( linetmp[0] == 'F') ||
			( linetmp[0] == 'L') ||
			( linetmp[0] == 'l')	) ++labnumb;
	}
	
	if(debufcount>1) fprintf( stderr, "find %u items" , itemsnumb );
	if(debufcount>1) fprintf( stderr, " %u label\n" , labnumb );
}

/*-----------------------------------------------*/

static void * readdata ( void ){
	
	uint32_t nrinlist, nrfun;
	int16_t stckb[2];

	faw = funwsk;
	
	nrinlist = 0;
	nrfun = 0;
	stckb[0] = 0;
	stckb[1] = 0;
	
	while ( 0 < getline ( &linetmp , &linesiz, fileldf ) ) {
		char chf, *wsl;
		
		chf = linetmp[0];
		faw->what = chf;
		
		wsl = &linetmp[2];
		
		faw->name[0] = 0;
		faw->stack = 0;
		faw->regist = 0;
		
		if( ( chf != 'e') && ( chf != 'r' ) && ( chf != 'p' ) )
			wsl = getnwathever ( faw->name, &linetmp[2], 32 );
		
		wsl = GETNUMB( faw->stack, wsl );
		wsl = GETNUMB( faw->regist, wsl );

		stckb[0] = faw->stack;
		
		/* if(nrinlist) faw->stack = stckb[1] - faw->stack; */
		if(nrinlist) faw->stack -= stckb[1];
		stckb[1] = stckb[0];
		
		if( ( chf == 'e') || ( chf == 'r' ) )
			stckb[1] = 0;
		
		/* if funkcion */
		if ( chf == 'F' ) {
			uint32_t adr = 0;
			funsum[nrfun].nronfaw = nrinlist;
			wsl = GETNUMB( adr, wsl );
			wsl = GETNUMB( adr, wsl );
			funsum[nrfun].adr = adr;
			funsum[nrfun].stckrfun = (int16_t)0x8000;
			funsum[nrfun].stckxfun = 0;
			funsum[nrfun].stckxall = 0;
			funsum[nrfun].stckhard = 0;
			funsum[nrfun].done = 0;
			if(debufcount>2) {
				fprintf( stderr, "%16s" , faw->name );
				fprintf( stderr, " %3u %3u 0x%07x\n" , nrfun, nrinlist, adr );
			}
			++nrfun;
		}
		
		++faw;
		++nrinlist;
	}
	
	return funwsk;
}

/*-----------------------------------------------*/

static uint8_t gfopen(void){

	if(fnmap) {
		filem = fopen ( fnmap, "r" );
		if ( ( filem == NULL ) && (debufcount) ) 
			fprintf( stderr, "bad open map file %s\n" , fnmap );
	} else	
		filem = NULL;

	if ( filem && debufcount )
		fprintf( stderr, "open map file: %s\n" , fnmap );

	
	fileldf = fopen ( "_lisdfun.txt", "r" );
	if( fileldf == NULL ) {
		if(debufcount)
			fprintf( stderr, "bad open file %s\n" , "_lisdfun.txt" );
		return 1;
	}
	
	if ( fileldf && debufcount )
		fprintf( stderr, "open file: %s\n" , "_lisdfun.txt" );
	
	return 0;
}

/*-----------------------------------------------*/
/*                                               */
/*-----------------------------------------------*/
