/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/* ---------------------------------------------------------- */
/*                                                            */
/*                   is string b in     a                     */
/*                                                            */
/* ---------------------------------------------------------- */

#include <stdint.h>
#include <stdio.h>

#include "ptcutil.h"

/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */


unsigned char hashexdigit ( unsigned char tmp2 ) {
	if ( ( tmp2 >= '0' ) && ( tmp2 <= '9' ) ) return tmp2 - ('0'-0);
	if ( ( tmp2 >= 'a' ) && ( tmp2 <= 'f' ) ) return tmp2 - ('a'-10);
	if ( ( tmp2 >= 'A' ) && ( tmp2 <= 'F' ) ) return tmp2 - ('A'-10);
	return 0x80;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*         change to upper                                    */
/*                                                            */
/* ---------------------------------------------------------- */

void setupper ( char * dst, char * src ) {
	
	while(src){
		char a;
		a = *src++;
		if ( (a>='a') && (a<='z') ) *dst++ = a-('a'-'A');
		else *dst++ = a;
		if(a==0) break;
	}
	
}

/* ---------------------------------------------------------- */
/*                                                            */
/*                   is string b in     a                     */
/*                                                            */
/* ---------------------------------------------------------- */

int szukaju ( char * a, char * b ) {

	int i =0;

	while ( ( *a != '\0' ) && ( *b != '\0' ) ) {

	  for ( i=0; i>=0; i++ ) {
		 if ( a[i] != b[i] ) {
			if ( ( a[i] != 0 ) && ( b[i] != 0 ) ) break;   /* nei ma szukamy dalej */    
			if ( ( a[i] == 0 ) && ( b[i] != 0 ) ) return -1; /* za dlugie b czylli nie ma */
			if ( b[i] == 0 ) return 0; /* jest b w a */
		 }
		 if ( b[i] == 0 ) return 0;   /* nei ma szukamy dalej */
	  }

	  a++;
	}

return 1;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*                   is string b in     a                     */
/*             get last point                                 */
/* ---------------------------------------------------------- */

char * getlastfind ( char * a, char * b ) {

	int i = 0;
	char *at, *bt;

	while(1){
		if(*a==0) break;
		bt = b;
		at = a;
		for ( i=0; i<250; i++ ) {
			if ( *bt == 0 ) return at;
			if ( *at != *bt ) break;
			++bt;
			++at;
		}
		++a;
	}

	return NULL;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*         find something and copy                            */
/*                                                            */
/* ---------------------------------------------------------- */

uint16_t getwathever (char *gdzie, char *co ) {
	uint16_t ile = 0;
		
	if(co==NULL) return 0;
	
	while(1) {
		if(*co>0x20) break;
		if(*co==0) return 0;
		++co;
	}
	
	while(1) {
		*gdzie = *co;
		if (*co<33) break;
		++gdzie;
		++co;
		++ile;
	}

	*gdzie = 0;
	
	return ile;	
}

/* ---------------------------------------------------------- */
/*                                                            */
/*  find something and copy skip spaces until , ;                  */
/*                                                            */
/* ---------------------------------------------------------- */

char * getargasm (char *gdzie, char *co, uint16_t max ) {
	
	uint16_t ile = 0;
		
	if(co==NULL) return NULL;
	
	while(1) {
		if(*co==0) return NULL;
		if(*co==',') return NULL;
		if(*co==';') return NULL;
		if(*co>0x20) break;
		++co;
	}
	
	while(1) {
		*gdzie = *co++;
		if (*gdzie==' ') continue;
		if (*gdzie==',') break;
		if (*gdzie==';') break;
		if (*gdzie<0x20) break;
		if( ++ile == max ) {
			*gdzie = 0;
			return NULL;
		};
		++gdzie;
	}
	
	*gdzie = 0;
	
	return co;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*         find something and copy                            */
/*                                                            */
/* ---------------------------------------------------------- */

char * getnwathever (char *gdzie, void *c, int max ) {
	uint16_t ile = 0;
	unsigned char *co = c;
	
	if(co==NULL) return NULL;
	if(max<2) return NULL;
	
	while(1) {
		if(*co==0) return NULL;
		if(*co>0x20) break;
		++co;
	}
	
	while(1) {
		*gdzie = *co;
		if (*co<33) break;
		if( ++ile == max ) {
			*gdzie = 0;
			return NULL;
		};
		++gdzie;
		++co;
	}

	*gdzie = 0;
	
	return (char*) co;
}


/* ---------------------------------------------------------- */
/*                                                            */
/*         find number                                        */
/*                                                            */
/* ---------------------------------------------------------- */

char * getnumnwathever ( void * dst, char * co, int max ) {
	
	uint8_t tmp, *ddd, znak;
	uint32_t insnum;
	
	if ( co == NULL ) return NULL;
	
	insnum = 0;
	znak = 0;
	
	while(1) {
		if(*co==0) return NULL;
		if( (*co>='0') && (*co<='9') ) break;
		if( (*co>='A') && (*co<='Z') ) break;
		if( (*co>='a') && (*co<='z') ) break;
		if( *co=='-' ) { ++co; znak = 1; break; }
		++co;
	}

	if((co[0]=='0')&&(co[1]=='x')){
		co += 2;
		while(1){
			tmp = hashexdigit ( *co );
			if(tmp>0x0F) break;
			insnum <<= 4;
			insnum += tmp;
			++co;
		}
	} else {
		while(1){
			tmp = hashexdigit ( *co );
			if(tmp>0x09) break;
			insnum *= 10;
			insnum += tmp;
			++co;
		}
	}
	
	if ( znak  ) {
		insnum = ~insnum;
		insnum += 1	;
	}
	
	ddd = dst;
	while(max--) {
		*ddd++ = insnum&0xFF;
		insnum >>= 8;
	}
	
	return co;
}

/* ---------------------------------------------------------- */
/*                                                            */
/*         find something and copy                            */
/*                                                            */
/* ---------------------------------------------------------- */

char * getsymnwathever (char *gdzie, char *co, int max ) {
	uint16_t ile = 0;
	
	if(co==NULL) return NULL;
	if(max<2) return NULL;
	
	while(1) {
		if(*co==0) return NULL;
		if( (*co>='0') && (*co<='9') ) break;
		if( (*co>='A') && (*co<='Z') ) break;
		if( (*co>='a') && (*co<='z') ) break;
		if(*co=='_') break;
		++co;
	}
	
	while(1) {
		*gdzie = *co;
		while(1){
			if( (*co>='0') && (*co<='9') ) break;
			if( (*co>='A') && (*co<='Z') ) break;
			if( (*co>='a') && (*co<='z') ) break;
			if(*co=='_') break;
			*gdzie = 0;
			return co;
		}
		if( ++ile == max ) {
			*gdzie = 0;
			return NULL;
		};
		++gdzie;
		++co;
	}

	*gdzie = 0;
	
	return (char*) co;
}

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

int readstr ( char * a, char * b, size_t s ) {

	size_t i = 0;

	while ( ++i < s ) {
	  if ( *b == 0 )
		 break;
	  *a++ = *b++;
	}

	*a = 0;

	return i - 1;
}

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

void * sah2b ( void * bin, const char * str, uint8_t size ) {
	
	uint8_t lstr;
	unsigned char tmp3;
	unsigned char * lb;

	lb = (unsigned char*) bin;
	lstr = 0;
	tmp3 = 0;

	if( !str ) return (void*)0;

	while (1) {
		tmp3 = *str;
		if ( !tmp3 ) return (void*)0;
		if ( (hashexdigit(tmp3) & 0x80) == 0 ) break;
		++str;
	}	
	for ( lstr=0; tmp3; ++lstr, ++str ) {
		tmp3 = *str;
		if ( hashexdigit(tmp3) & 0x80 ) break;
	}
	
	bin = (void*) str;
	
	while ( size && lstr-- ) {
		tmp3 = hashexdigit(*--str);
		--size;
		
		if ( lstr == 0 ) {
			*lb++ = tmp3;
			break;
		}
		tmp3 |= hashexdigit(*--str) << 4;
		*lb++ = tmp3;
		--lstr;
	}

	while (size--) *lb++ = 0;
	
	return bin;
}


/* if pointer is hexdigit return hex */

char * getnumbhex ( void * dst, char * co, int max ) {

	uint32_t insnum;
	uint8_t tmp, *ddd;
	
	insnum = 0;
	tmp = hashexdigit ( *co );
	
	while( 0x10 > tmp ) {
		insnum <<= 4;
		insnum += tmp;
		++co;
		tmp = hashexdigit ( *co );
	}
	
	if(*co != ':') return NULL;

	ddd = dst;
	
	while(max--) {
		*ddd++ = insnum&0xFF;
		insnum >>= 8;
	}
	
	return co;
}

/* ---------------------------------------------------------- */
/*            end file                                        */
/* ---------------------------------------------------------- */
