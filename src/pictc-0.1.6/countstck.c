/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/* ---------------------------------------------------------- */
/*
1. find top funkcion
2. count everything from top
*/
/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "ptcutil.h"

/* ---------------------------------------------------------- */

#define MAXFUN 65536

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

static char *linetmp;
static size_t linesiz = 1024;

struct fun {
	char name[256];
	char what;
	int16_t stack;
	uint8_t regist;
};

/* ---------------------------------------------------------- */

struct funres {
	uint32_t nrfun;
	int16_t stack;
	uint8_t rcall;
	uint8_t regist;
	uint8_t type;
};

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

uint8_t findgglobjc( struct fun * f, char *n ) {

	while( f ) {
		if ( 'E' == f->what) break;
		if (( 'C' == f->what) || ( 'G' == f->what) ){
			if ( 0 == strcmp( f->name, n ) ) return 0;
		}
		++f;
	}

	return 1;
}

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

uint32_t getglobg( struct fun * f, char *n ) {
	
	uint32_t nrglob = 0;

	while( f ) {
		if ( 'E' == f->what) break;
		if ( 'L' == f->what) {
			if ( 0 == strcmp( f->name, n ) ) return nrglob;
		}
		++f;
		++nrglob;
	}

	return 0;
}

/* ---------------------------------------------------------- */
/*  return label local                                        */
/* ---------------------------------------------------------- */

uint32_t getlocc( struct fun * f, char *n, uint32_t nr ) {
	
	f += nr;
	
	/* find f */
	while(1) {
		--f;
		if(--nr==0) return 0;
		if ( 'f' == f->what) break;
	}
	
	while(1) {
		++f;
		++nr;
		if ( 'l' == f->what) {
			if ( 0 == strcmp( f->name, n ) ) return nr;
		}
		if ( 'f' == f->what) return 0;
	}
	
	return 0;
}

/* ---------------------------------------------------------- */
/*  return label outside fun                                  */
/* ---------------------------------------------------------- */

uint32_t getlocg( struct fun * f, char *n, uint32_t nr ) {
	
	struct fun * bf;
	uint8_t out = 0;
	uint16_t nrglob = nr;
	
	bf = f;
	
	/* back find m */
	f += nr;
	
	while(1) {
		--f;
		--nr;
		if(nr==0) return 0;
		if ( 'f' == f->what) break;
		if ( 'm' == f->what ) out = 1;
		if ( 'l' == f->what) {
			if ( 0 == strcmp( f->name, n ) ) {
				if( out ) return nr;
				else return 0;
			}
		}
	}
	
	nr = nrglob;
	f = bf + nr;
	out = 0;
	
	while(1) {
		++f;
		++nr;
		if(nr==0) return 0;
		if ( 'f' == f->what) return 0; /* this shouldn happen */
		if ( 'e' == f->what ) out = 1;
		if ( 'l' == f->what) {
			if ( 0 == strcmp( f->name, n ) ) {
				if( out ) return nr;
				else return 0;
			}
		}
	}

	return 0;
}

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

uint8_t findlocaljc( struct fun * f, char *n ) {
	
	while( f ) {
		if ( 'E' == f->what) break;
		if ( 'f' == f->what ) return 1;
		if (( 'c' == f->what) || ( 'g' == f->what) ){
			if ( 0 == strcmp( f->name, n ) ) return 0;
		}
		++f;
	}

	return 1;
}

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

void listmax ( struct fun * f, uint32_t * l, uint32_t n ) {
	
	int16_t maxstack;

	f += n;
	l += n;
	
	while(n--) {
		--f;
		--l;
		if( 'e' == f->what ) { 
			getnumnwathever ( &maxstack, f->name, sizeof(maxstack) );
			/*
			maxstack = f->stack;
			maxreg = f->regist;
			fprintf( stderr, "%i %i\n" , maxstack, maxreg );*/
		}
		if( 'm' == f->what ) maxstack = -1;
		*l = maxstack;
	}

}

/* ---------------------------------------------------------- */
/*                                                            */
/*  ivec_0x1_tc_int                                           */
/*  _tch_int                                                  */
/*                                                            */
/* ---------------------------------------------------------- */

const char irqh[] = "ivec_0x1_tch_int";
const char irql[] = "_tc_int";
const char irql1[] = "ivec_0x2_tc_int";

/* ---------------------------------------------------------- */

extern uint8_t debufcount;

/* ---------------------------------------------------------- */

uint8_t isonthelist ( uint32_t *list, uint32_t what, uint32_t max ) {
	
	while(max--)
		if( what == list[max] ) return 1;

	return 0;
}

/* ---------------------------------------------------------- */


uint8_t cs ( void ) {

	FILE *filelist;
	struct fun * funasm, *faw;
	uint32_t startb, i, nfaw, nftop;
	uint32_t *listnt, nrlistnt;
	int16_t *listms;
	char chf, *wsl;
	uint8_t dblvl;
	uint8_t state;

	uint16_t *listcall, nrcall, nrcallmax;
	uint32_t nrpath, *listpath;
	uint32_t nrwork, nrlist;
	int16_t stackin, stackmax, regmax;
	int16_t stb;
	struct funres * result, *wsfh, *wsfl;
	struct funres * wrf;	
	
	funasm = malloc(MAXFUN*sizeof(*funasm));	
	listnt = malloc(MAXFUN*sizeof(*listnt));
	listnt[0] = 0;
	nrlistnt = 0;
	
	dblvl = 2;
	
	/* list diff value stack */
	listms = malloc(MAXFUN*sizeof(*listms));
	
	/* open list file */
	filelist = fopen ( "_listnamefun.txt", "r" );
	if(filelist==NULL){
		fprintf( stderr, "bad open: %s\n" , "_listnamefun.txt" );
		return 1;
	} else {
		if ( dblvl > 3 )
			fprintf( stderr, "open: %s\n" , "_listnamefun.txt" );
	}
		
	linetmp = malloc(1024);
		
	nfaw = 0;
	faw = funasm;
	
	/* read */
	while(1){
		if( 0 > getline ( &linetmp , &linesiz, filelist ) ) break;
		
		chf = linetmp[0];
		faw->what = chf;
		wsl = getnwathever ( faw->name, &linetmp[2], 256 );
		wsl = getnumnwathever ( &faw->stack, wsl, sizeof(faw->stack) );
		if('e'==chf) wsl = getnumnwathever ( &faw->regist, wsl, sizeof(faw->regist) );
		
		/*
		fprintf ( stderr, "%c %i %s\n", faw->what, faw->stack, faw->name );
		*/
		++faw;
		++nfaw;
	}
	faw->what = 'E';
	fclose(filelist);
	
	if ( dblvl > 3 )
		fprintf ( stderr, "%u\n", nfaw);
	
		
	faw = funasm;
	
	/* find top */
	/* top are lable first after m which has no c,g */
	state = 0;
	startb = 0;
	
	for( nftop=0, i=0; i<nfaw; ++i , ++faw ) {
		
		char ntf[256];
		/*
		fprintf ( stderr, "%c %i %s\n", faw->what, faw->stack, faw->name );
		*/
		if(faw->what=='f') {
			startb = i+1;
			state = 1;
			continue;
		} /* new local visible */
		
		if(faw->what=='m') {
			state = 2;
			continue;
		} /* new block code */
		
		if(faw->what=='L') {
			if ( state & 4 ) continue;
			if(debufcount)fprintf ( stderr, "%c %i %s\n", faw->what, faw->stack, faw->name );
			state |= 4;
			readstr ( ntf, faw->name, 256 );
			if ( findgglobjc( funasm, faw->name ) ) { /* top is */
				++nftop;
				listnt[nrlistnt++] = i;
				listnt[nrlistnt] = 0;
			}
			continue;
		} /* global label find global G C */
		
		if (faw->what=='l') {
			if ( state & 4 ) continue;
			if(debufcount)fprintf ( stderr, "%c %i %s\n", faw->what, faw->stack, faw->name );
			state |= 4;
			readstr ( ntf, faw->name, 256 );
			if ( findlocaljc( funasm+startb, faw->name ) ) { /* top is */
				++nftop;
				listnt[nrlistnt++] = i;
				listnt[nrlistnt] = 0;
			}
			continue;
		} /* local label find local g c */
	
		/*
		if(faw->what=='e') {continue;}
		if(faw->what=='C') {continue;}
		if(faw->what=='c') {continue;}
		if(faw->what=='G') {continue;}
		if(faw->what=='g') {continue;}
		*/
		
		if( 'E' == faw->what) break;
	}
	
	/* print what find */
	for( nrlistnt=0; ; ++nrlistnt ) {
		if ( 0 == listnt[nrlistnt] ) break;
		faw = funasm + listnt[nrlistnt];
		if ( dblvl > 3 )
			fprintf ( stderr, "%c %i %s\n", faw->what, faw->stack, faw->name );
	}
	
	/* find path and max */

	listpath = malloc(MAXFUN*sizeof(*listpath));
	nrpath = 0;
	listpath[nrpath] = 0;
	
	result = malloc(nftop*sizeof(*result));
	
	listcall = malloc(MAXFUN*sizeof(*listcall));
	nrcall = 0;
	listcall[nrcall] = 0;
	
	/* get fist element */
	
	faw = funasm;
	
	for ( nrwork = 0; nrwork<nfaw; ++nrwork ) {
		
		if(( faw->what == 'f' ) ||
			( faw->what == 'e' ) ||
			( faw->what == 'E' ) ||
			( faw->what == 'm' ) ) {
			
			++faw;
			continue;
		}
		
		stackin = -faw->stack;
		++faw;
		stackin += faw->stack;

		listms[nrwork] = stackin;
		
		if ( dblvl > 3 )
			fprintf ( stderr, "fun %u : %s %i\n", nrwork, faw->name, listms[nrwork]);
	}
	
	
	nrlist = 0;
	nrwork = listnt[nrlist];
	faw = funasm + nrwork;
	
	listms[nrwork] = 0;
	stackmax = 0;
	nrcallmax = 0;
	stb = 0;
	regmax = 0;
	
	wsfh = NULL;
	wsfl = NULL;
	
	faw = funasm + nrwork;
	listpath[nrpath++] = nrwork;
	
	if ( dblvl > 3 )
		fprintf ( stderr, "\n-new fun %u : %s\n", nrwork, faw->name);
	
	if ( dblvl > 3 )
		fprintf ( stderr, "%c: %3u %3u stck %3i\n", faw->what, nrwork, nrpath, listms[nrwork] );

	/* get next element */
	if ( ++nrwork >= nfaw ) {
		if ( dblvl > 3 )
			fprintf ( stderr, "end list\n" );
		return 0;
	}
		

	while(1) {
		uint32_t tmp;
	
		tmp = 0;
		faw = funasm + nrwork;

		/* if G means goto global */
		if ( faw->what == 'G' ) {
			
			tmp = getglobg( funasm, faw->name );
		
			if (tmp == 0 ) {
				++nrwork;
				if ( dblvl > 2 )
					fprintf ( stderr, "goto global not found %20s ...skip\n", faw->name );
				if ( dblvl > 2 )
					fprintf ( stderr, "found %3u stackval %3u\n", nrwork, listms[nrwork] );
			}
			else {
				stb += listms[nrwork]; /* stack value */
				if(stackmax<stb) stackmax = stb;
				if ( dblvl > 3 )
					fprintf ( stderr, "G: %3u %3u stck %3i\n", nrwork, nrpath, listms[nrwork] );
				nrwork = tmp;
			}
			continue;
		}
		
		/* if L means global label gate next */
		if ( faw->what == 'L' ) {
			/* if is back */
			if( isonthelist ( listpath, nrwork, nrpath ) ) goto __FLOW_BACK__;
				
			stb += listms[nrwork]; /* stack value */
			if(stackmax<stb) stackmax = stb;
			if ( dblvl > 3 )
				fprintf ( stderr, "L: %3u %3u stb %3i\n", nrwork, nrpath, stb );
			listpath[nrpath++] = nrwork;
			++nrwork;
			continue;
		}
		
		if ( faw->what == 'C' ) { tmp = getglobg( funasm, faw->name ); }
		if ( faw->what == 'c' ) { tmp = getlocc( funasm, faw->name, nrwork ); }
		
		/* if L means global label gate next */
		if ( ( faw->what == 'C' ) || ( faw->what == 'c' ) ) {
			if (tmp == 0 ) {
				stb += listms[nrwork]; /* stack value */
				if ( dblvl > 2 )
					fprintf ( stderr, "call global not found %20s ...skip\n", faw->name );
				listpath[nrpath++] = nrwork;
				++nrwork;
			}
			else {
				/* stack call */
				if ( dblvl > 3 )
					fprintf ( stderr, "C: %3u %3u stb %3i", nrwork, nrpath, stb );
				listcall[++nrcall] = nrwork;
				listpath[nrpath++] = nrwork;
				if( nrcallmax < nrcall ) nrcallmax = nrcall;
				nrwork = tmp;
				if ( dblvl > 3 )
					fprintf ( stderr, " %3u cmax %3u\n", nrwork, nrcallmax );
			}
			continue;
		}
				
		/* if l means local label get next */
		if ( ( faw->what == 'l' ) || ( faw->what == 'i' ) ) {
			if( isonthelist ( listpath, nrwork, nrpath ) ) goto __FLOW_BACK__;
			stb += listms[nrwork]; /* stack value */
			if(stackmax<stb) stackmax = stb;
			if ( dblvl > 3 )
				fprintf ( stderr, "l: %3u %3u stb %3i\n", nrwork, nrpath, stb );
			listpath[nrpath++] = nrwork;
			++nrwork;
			continue;
		}
		
		/* if g means local label -> if the same funkcion tracted like label */
		/* other way tracted like call but without ++nrcall */
		if ( faw->what == 'g' ) {
			tmp = getlocg( funasm, faw->name, nrwork );
			if ( dblvl > 3 )
				fprintf ( stderr, "g found %3u %3u l %3u\n", nrwork, nrpath, tmp );
			if(tmp==0){ /* like label */
				stb += listms[nrwork]; /* stack value */
				if(stackmax<stb) stackmax = stb;
				listpath[nrpath++] = nrwork;
				++nrwork;
			} else { /* like call */
				listms[nrwork] = 0;
				nrwork = tmp;
			}
			continue;
		}
		
		/* push value inc stack hard */
		if (faw->what=='p') {
			stb += listms[nrwork]; /* stack value */
			if(stackmax<stb) stackmax = stb;
			++nrcall;
			if( nrcallmax < nrcall ) nrcallmax = nrcall;
			--nrcall;
			++nrwork;
			continue;
		}
		
		/* if e -> end counting if not top return and compare with max */
		if ( faw->what == 'e' ) {
			
			__FLOW_BACK__:

			if( regmax < faw->regist) regmax = faw->regist;
			
			if ( dblvl > 3 )
					fprintf ( stderr, "e: %3u %3u stb %3i\n", nrwork, nrpath, stb );
			
			while(nrpath) {
				nrwork = listpath[--nrpath];
				
				/* if back to call break */
				faw = funasm + nrwork;
				
				if( listcall[nrcall] == nrwork ) {
					stb += listms[nrwork]; /* stack value */
					if(stackmax<stb) stackmax = stb;
					--nrcall;
					if ( dblvl > 3 )
						fprintf ( stderr, ":c %3u -> ", nrwork );
					if ( dblvl > 3 )
						fprintf ( stderr, " : %3u b %3i x %3i l %3i\n",
							nrwork, stb, stackmax, listms[nrwork] );
					++nrwork;
					++nrpath;
					break;
				}
				if ( dblvl > 3 )
					fprintf ( stderr, ":  %3u b %3i x %3i l %3i :",
						nrwork, stb, stackmax, listms[nrwork] );
				if ( dblvl > 3 )
					fprintf ( stderr, "b: %3u %3u %3i %3i\n", 
						nrpath, nrwork, (int16_t)listms[nrwork], stackmax );
			}
			
			/* new funkcion */
			if( nrpath == 0 ) {
				struct funres * wrf;
				/*
				fprintf ( stdout, "%30s %3u %3u %3u\n",
					faw->name, stackmax, nrcallmax, regmax );
				*/
				wrf = result + nrlist;
				wrf->nrfun = nrwork;
				wrf->stack = stackmax;
				wrf->rcall = nrcallmax;
				wrf->regist = regmax;
				
				if ( 0 == strcmp( faw->name, irql ) ) wsfl = wrf;
				if ( 0 == strcmp( faw->name, irql1 ) ) wsfl = wrf;
				if ( 0 == strcmp( faw->name, irqh ) ) wsfh = wrf;

				if ( dblvl > 3 )
					fprintf ( stderr, "-end fun %u : %s stb %i\n\n",
						nrwork, faw->name, stb );
				
				++nrlist;
				nrwork = listnt[nrlist];
				faw = funasm + nrwork;
				if ( dblvl > 3 )
					fprintf ( stderr, "-new fun %u : %s\n", nrwork, faw->name);
				if ( nrwork == 0 ) {
					if ( dblvl > 3 )
						fprintf ( stderr, "-end list-\n" );
					break;
				}
				nrcall = 0;
				nrcallmax = 0;
				stackmax = 0;
				stb = 0;
				regmax = 0;
			}

			continue;
		}
		
		/* if is fun ine the lsit continue */
		/* else  */
		
		break;
	}
	
	/* summary */
	
	fprintf ( stdout, "%30s", "name" );
	fprintf ( stdout, " sstck" );
	fprintf ( stdout, "  regf" );
	fprintf ( stdout, " hstck" );
	fprintf ( stdout, "  +l+h" );
	fprintf ( stdout, "   ?lh" );
	fprintf ( stdout, " sumhs" );
	fprintf ( stdout, "\n" );
	
	for( wrf = result, i=0; i<nftop; ++i, ++wrf ) {
		int16_t tmp;
		
		faw = funasm + wrf->nrfun;
		
		fprintf ( stdout, "%30s", faw->name);
		fprintf ( stdout, " %5u", wrf->stack );
		fprintf ( stdout, " %5u", wrf->regist );
		fprintf ( stdout, " %5u", wrf->rcall );

		if( ( NULL == wsfl ) && ( NULL == wsfh ) ) {
			fprintf ( stdout, "\n" );
			continue;
		}
			
		if( ( wrf != wsfl ) && ( wrf != wsfh ) ) {
			int16_t tmp;
			
			tmp = wrf->stack;
			if( wsfh ) tmp += wsfh->stack;
			if( wsfl ) tmp += wsfl->stack;
			fprintf ( stdout, " %5u", tmp );
			
			if( wsfh ) tmp = wsfh->stack;
			if( wsfl ) {
				if( tmp < wsfl->stack )
				tmp = wsfl->stack;
			}
			
			tmp += wrf->stack;
			fprintf ( stdout, " %5u", tmp );
			
		} else fprintf ( stdout, "      " );
		
		tmp = wrf->rcall + 2;
		if( wsfh ) tmp += wsfh->rcall;
		if( wsfl ) tmp += wsfl->rcall;
		fprintf ( stdout, " %5u", tmp );
		
		fprintf ( stdout, "\n" );

	}
	
	
	return 0;
}


/* ---------------------------------------------------------- */
/*        endfile                                             */
/* ---------------------------------------------------------- */
