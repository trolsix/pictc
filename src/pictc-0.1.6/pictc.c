/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018-2020 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/* -----------------------------------------------

  asm analyzer

----------------------------------------------- */

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "ptcutil.h"
#include "list.h"

/* ---------------------------------------------------------- */

const char pictcver[] = "pictc ver 0.1.6";

/* ---------------------------------------------------------- */

void cs ( void );
unsigned int lookpicdasm ( char ** );
uint8_t dcs ( void );

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

static char *linetmp;
static size_t linesiz = 1024;

struct listg lgloball, lfunglob;

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

uint8_t flowopt;
	
char *fndasm;
char *fnmap;

int readarg ( int argc, char *argv[] );

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

enum { code, postdec, gptput, preinc, postinc };

#define CODESET() ( flag |= 1<<code )
#define CODECLR() ( flag &= ~(1<<code) )
#define CODEIS() (flag&(1<<code))

#define POSTDEC1SET() ( flag |= 1<<postdec )
#define POSTDEC1CLR() ( flag &= ~(1<<postdec) )
#define POSTDEC1IS() (flag&(1<<postdec))

#define PREINC1SET() ( flag |= 1<<preinc )
#define PREINC1CLR() ( flag &= ~(1<<preinc) )
#define PREINC1IS() (flag&(1<<preinc))

#define POSTINC1SET() ( flag |= 1<<postinc )
#define POSTINC1CLR() ( flag &= ~(1<<postinc) )
#define POSTINC1IS() (flag&(1<<postinc))

#define GPTPUTSET() ( flag |= 1<<gptput )
#define GPTPUTCLR() ( flag &= ~(1<<gptput) )
#define GPTPUTIS() (flag&(1<<gptput))

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

#define lenghtname 128

uint8_t debufcount = 0;

uint8_t lookpicasm ( char **args ) {
	
	FILE * plik, *filelist;
	uint8_t maxregfun, maxregreg;
	uint8_t dbg;
	char *pstru;
	
	char *codename;
	char *name;
	char *what;
	char *what2;
	char *afterupper;
	
	uint32_t value;
	uint16_t nrpush;
	uint32_t nrip;
	uint16_t resram, resstack, ressum;
	uint8_t valw;
	int16_t numberdec, numberdecmax, numberdecloc, numberdecmaxall;
	int16_t dacmaxsave;
	uint32_t flag;
	
	args++;
	dbg = 0;
	
	codename = malloc(lenghtname);
	name = malloc(lenghtname);
	what = malloc(lenghtname);
	what2 = malloc(lenghtname);
	afterupper = malloc(lenghtname);
	
	nrpush = 0;
	nrip = 0;
	flag = 0;
	resram = 0;
	ressum = 0;
	numberdecloc = 0;
	numberdecmaxall = 0;
	plik = NULL;
	
	/* open list file */
	filelist = fopen ( "_listnamefun.txt", "w" );
	
	addglf ( &lgloball, NULL, listnew );
	addglf ( &lfunglob, NULL, listnew );

	linetmp = malloc(linesiz);

	while (1) {
		
		args++;
		if(*args==NULL) break;
		if(plik) fclose(plik);
		plik = fopen ( *args, "r" );
		if(plik==NULL){
			fprintf( stderr, "bad open: %s\n" , *args );
			return 1;
		} else {
			fprintf( stdout, "open: %s\n" , *args );
		}
		
		if( filelist )
			fprintf( filelist, "f %s\n" , *args );
		
		CODECLR();
		
		resstack = 0;
		numberdec = 0;
		numberdecmax = 0;
		maxregfun = 0;
		maxregreg = 0;
		dacmaxsave = 0;
		valw = 0;
		resram = 0;
		
		addglf ( &lgloball, NULL, listclr );
		addglf ( &lfunglob, NULL, listclr );
		
		fprintf( stdout, "%40s", "codename" );
		fprintf( stdout, " sstck" );
		fprintf( stdout, "  regx" );
		fprintf( stdout, "  regf" );
		fprintf( stdout, "  ramf" );
		fprintf( stdout, "  rams" );
		fprintf( stdout, " resstack\n" );
	
		while(1){
			if( 1 > getline ( &linetmp , &linesiz, plik ) ) {
				break;
			}
			name[0] = 0;
			what[0] = 0;
			what2[0] = 0;
			
			pstru = getnwathever (name, linetmp, lenghtname );
			pstru = getsymnwathever (what, pstru, lenghtname );
			pstru = getsymnwathever (what2, pstru, lenghtname );
			/* if(pstru==NULL) continue; */
			if ( name[0] == ';' ) continue;
			
			if ( 0 == strcmp( "global", name ) ) {
				addglf ( &lgloball, what, nameadd );
				if(debufcount) fprintf( stderr, "g %s\n" , what );
				continue;
			}
			
			if ( 0 == strcmp( "extern", name ) ) {
				addglf ( &lgloball, what, nameadd );
				if(debufcount) fprintf( stderr, "e %s\n" , what );
				continue;
			}

			setupper ( afterupper, name );
			if ( 0 == strcmp( "PUSH", name ) ) {
				fprintf( filelist, "p _*%u*_ %i\n" , nrpush++, numberdecloc );
				continue;
			}
			
			/* count reserved memory */
			/* _stack and _stack_end */
			if ( 0 == strcmp( "res", what ) ) {
				pstru = getnumnwathever (&value, what2, sizeof(value) );
				if(pstru==NULL) continue;
				if ( 0==strcmp( "_stack", name ) ) {
					resstack += value;
					continue;
				}
				if ( 0==strcmp( "_stack_end", name ) ) {
					resstack += value;
					continue;
				}
				if ( 0==strncmp( "r0x", name, 3 ) ) {
					uint16_t nreg;
					pstru = getnumnwathever (&nreg, &name[1], sizeof(nreg) );
					nreg += 1;
					if (nreg>maxregreg)maxregreg = nreg;
					continue;
				}
				
				resram += value;
				ressum += value;
				
				/* fprintf( stderr, "_%s_%u_\n" , name, value ); */
			}
			
			
			if ( 0==strncmp( "r0x", what, 3 ) ) {
				uint16_t nreg;
				pstru = getnumnwathever (&nreg, &what[1], sizeof(nreg) );
				nreg += 1;
				if (nreg>maxregfun)maxregfun = nreg;
			}
			
			if ( 0==strncmp( "r0x", what2, 3 ) ) {
				uint16_t nreg;
				pstru = getnumnwathever (&nreg, &what2[1], sizeof(nreg) );
				nreg += 1;
				if (nreg>maxregfun)maxregfun = nreg;
			}
				
			/* code */
			if (  0 == strcmp( "code", what ) ) {

				if( CODEIS() ) {
					if ( (numberdecloc>numberdec) && (numberdecloc>dacmaxsave) ){
						fprintf( filelist, "i _*%u*_ %i\n" , nrip++, numberdecloc );
						if( dacmaxsave < numberdecloc ) dacmaxsave = numberdecloc;
						numberdecloc = numberdec;
					}
					
					addglf ( &lfunglob, NULL, listclr );
					
					fprintf( stdout, "%40s", codename );
					fprintf( stdout, "%5u", numberdecmax );
					fprintf( stdout, " %5u", maxregreg );
					fprintf( stdout, " %5u", maxregfun );
					fprintf( stdout, " %5u %5u %5u\n", resram, ressum, resstack );
					
					if( filelist )
						fprintf( filelist, "e %i %i %u\n" , numberdecmax, numberdec, maxregfun );
				
				}
				/* fprintf( stderr, "_%s_\n" , name ); */

				CODESET();
				numberdec = 0;
				numberdecmax = 0;
				numberdecloc = 0;
				dacmaxsave = 0;
				maxregfun = 0;
				valw = 0;
				
				getnwathever ( codename, name, lenghtname );
				
				if( filelist ) {
					fprintf( filelist, "m %s %i\n" , codename, numberdec);
				}
				
				continue;
			}
			
			
			/* if what is posdec inc numberdec */
			POSTDEC1CLR();
			setupper ( afterupper, what2 );
			if ( 0 == strcmp( "POSTDEC1", afterupper ) ) POSTDEC1SET();
			if ( 0 == strcmp( "_POSTDEC1", afterupper ) ) POSTDEC1SET();
			setupper ( afterupper, what );
			if ( 0 == strcmp( "POSTDEC1", afterupper ) ) POSTDEC1SET();
			if ( 0 == strcmp( "_POSTDEC1", afterupper ) ) POSTDEC1SET();
			
			if(POSTDEC1IS()) {
				++numberdec;
				if(numberdecmax<numberdec) numberdecmax = numberdec;
				if(numberdecloc<numberdec) numberdecloc = numberdec;
				if(numberdecmaxall<numberdecmax) numberdecmaxall = numberdecmax;
				if(debufcount)
					fprintf( stderr, "BZ: %3u %3u %3i %s %s %s\n" ,
					numberdecmaxall, numberdecmax, numberdec, name, what, what2 );
				continue;
			}
			
			
			/* if what is posdec inc numberdec */
			POSTINC1CLR();
			setupper ( afterupper, what2 );
			if ( 0 == strcmp( "POSTINC1", afterupper ) ) POSTINC1SET();
			if ( 0 == strcmp( "_POSTINC1", afterupper ) ) POSTINC1SET();
			setupper ( afterupper, what );
			if ( 0 == strcmp( "POSTINC1", afterupper ) ) POSTINC1SET();
			if ( 0 == strcmp( "_POSTINC1", afterupper ) ) POSTINC1SET();
			
			if(POSTINC1IS()) {
				if ( numberdec == 0 ) {
					if(debufcount)
					fprintf( stderr, "stack <0 but don't worry yet ;)\n" );
				}
				--numberdec;
				if(numberdecmax<numberdec) numberdecmax = numberdec;
				if(numberdecloc<numberdec) numberdecloc = numberdec;
				if(numberdecmaxall<numberdecmax) numberdecmaxall = numberdecmax;
				if(debufcount)
					fprintf( stderr, "BZ: %3u %3u %3i %s %s %s\n" ,
					numberdecmaxall, numberdecmax, numberdec, name, what, what2 );
				continue;
			}
			
			/* if what is preinc */
			PREINC1CLR();
			setupper ( afterupper, what2 );
			if ( 0 == strcmp( "PREINC1", afterupper ) ) PREINC1SET();
			if ( 0 == strcmp( "_PREINC1", afterupper ) ) PREINC1SET();
			setupper ( afterupper, what );
			if ( 0 == strcmp( "PREINC1", afterupper ) ) PREINC1SET();
			if ( 0 == strcmp( "_PREINC1", afterupper ) ) PREINC1SET();
			
			if(PREINC1IS()) {
				if ( numberdec < 1 ) {
					if(debufcount)
					 fprintf( stderr, "stack <0 but don't worry yet ;)\n" );
				}
				--numberdec;
				if(numberdecmax<numberdec) numberdecmax = numberdec;
				if(numberdecloc<numberdec) numberdecloc = numberdec;
				if(numberdecmaxall<numberdecmax) numberdecmaxall = numberdecmax;
				if(debufcount)
					fprintf( stderr, "BZ: %3u %3u %3i %s %s %s\n" ,
					numberdecmaxall, numberdecmax, numberdec, name, what, what2 );
				continue;
			}
			
			

			

			
			setupper ( afterupper, name );
			
			if ( 0 == strcmp( "GOTO", afterupper ) ) {
				
				if( filelist ) {
					if ( (numberdecloc>numberdec) && (numberdecloc>dacmaxsave) ){
						fprintf( filelist, "i %s %i\n" , "_***_", numberdecloc );
						if( dacmaxsave < numberdecloc ) dacmaxsave = numberdecloc;
					}
					numberdecloc = numberdec;
					if ( nexist == addglf ( &lgloball, what, nexist ) )
						fprintf( filelist, "G %s %i\n" , what, numberdec );
					else fprintf( filelist, "g %s %i\n" , what, numberdec );
				}
				continue;
			}
			
			
			
			/* find call */
			if( ( 0 == strcmp( "RCALL", afterupper ) ) ||
			    ( 0 == strcmp( "CALL", afterupper ) ) ) {
				
				/* save what call */
				if( filelist ) {
					if ( (numberdecloc>numberdec) && (numberdecloc>dacmaxsave) ){
						fprintf( filelist, "i %s %i\n" , "_***_", numberdecloc );
						if( dacmaxsave < numberdecloc ) dacmaxsave = numberdecloc;
					}
					numberdecloc = numberdec;
					if ( nexist == addglf ( &lgloball, what, nexist ) )
						fprintf( filelist, "C %s %i\n" , what, numberdec );
					else fprintf( filelist, "c %s %i\n" , what, numberdec );
				}
				
				GPTPUTCLR();
				if (( 0 == strcmp( "__gptrput1", what )) ||
					( 0 == strcmp( "__gptrput2", what ) ) || 
					( 0 == strcmp( "__gptrput3", what ) ) ||
					( 0 == strcmp( "__gptrput4", what ) ) )  GPTPUTSET();
				
				if(GPTPUTIS()) {
					if(numberdec) --numberdec;
					if(debufcount) fprintf( stderr, "find gptr\n" );
				}
				
				/* save return call - fora better coinnt stack later */
				/*if( filelist ) fprintf( filelist, "r %s %i\n" , "*rc*", numberdec );
				zamiast etego bedzie zmienna */
				
				continue;
			}
			
			
			if ( 0 == strcmp( "MOVLW", afterupper ) ) {
				pstru = getnumnwathever (&valw, what, sizeof(valw) );
				if(debufcount) fprintf( stderr, "find movlw %u\n", valw  );
				continue;
			}
			
			
			if ( 0 == strcmp( "ADDWF", afterupper ) ) {
				if(debufcount) fprintf( stderr, "find fsr1 %s\n" , what );
				/* is modyfing fsr after call */
				setupper ( afterupper, what );
				if ( 0 == strcmp( "FSR1L", afterupper ) ) {
					if(valw<=numberdec) numberdec -= valw;
				}
				continue;
			}
			
			if ( 0 == strcmp( "SUBWF", afterupper ) ) {
				if(debufcount) fprintf( stderr, "find fsr1 %s %u\n" , what, valw );
				/* is modyfing fsr after call */
				setupper ( afterupper, what );
				if ( 0 == strcmp( "FSR1L", afterupper ) ) {
					numberdec += valw;
				}
				continue;
			}
			

			/* find jmp */
			if( ':' == name[strlen(name)-1] ) {
				name[strlen(name)-1] = 0;
				if ( (numberdecloc>numberdec) && (numberdecloc>dacmaxsave) ){
					fprintf( filelist, "i %s %i\n" , "_***_", numberdecloc );
					if( dacmaxsave < numberdecloc ) dacmaxsave = numberdecloc;
				}
				numberdecloc = numberdec;
				if ( nexist == addglf ( &lgloball, name, nexist ) ) {
					addglf ( &lfunglob, name, nameadd );
					if(dbg&0x04)
						fprintf( stderr, "label: %s\n" , name );
					fprintf( filelist, "L %s %i %s\n" , name, numberdec, codename );
				} else {
					if(dbg&0x04)
						fprintf( stderr, "label: %s\n" , name );
					fprintf( filelist, "l %s %i %s\n" , name, numberdec, codename );
				}
				continue;
			}
			
			if ( 0 == strcmp( "end", name ) ) {
				fprintf( stdout, "%40s", codename );
				fprintf( stdout, "%5u", numberdecmax );
				fprintf( stdout, " %5u", maxregreg );
				fprintf( stdout, " %5u", maxregfun );
				fprintf( stdout, " %5u %5u %5u\n", resram, ressum, resstack );
				
				if( filelist )
					fprintf( filelist, "e %i %i %u\n" , numberdecmax, numberdec, maxregfun );
				
				addglf ( &lfunglob, NULL, listclr );
				break;
			}
			
		}
		
		
	}

	
	return 0;
}


/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

int readarg ( int argc, char *argv[] );

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */


int main(int argc, char *args[]) {
	
	char *pstru;
	char name[64];
	char what[64];

	flowopt = 0;
	fndasm = NULL;
	fnmap = NULL;
	
	
	debufcount = 0;
	
	if( argc == 0 ) {
		return 0;
	}
	
	if((args[1][0] =='-') && (args[1][1] =='v')){
		fprintf( stderr, "%s\n" , pictcver );
		return 0;
	}
	
	if((args[1][0] =='-') && (args[1][1] =='a')){
		return (unsigned int) lookpicasm ( args );
	}
	
	if((args[1][0] =='-') && (args[1][1] =='s')){
		cs();
		return 0;
	}

	if((args[1][0] =='-') && (args[1][1] =='d')){
		readarg ( argc, args );
		flowopt = 1;
		return (unsigned int) lookpicdasm ( args );
	}
	
	if((args[1][0] =='-') && (args[1][1] =='D')){
		readarg ( argc, args );
		dcs();
		return 0;
	}

	/*  make automatic stack for pic18 read from linker script */
	/*
	find max DATABANK and read END
	DATABANK   NAME=gpr0       START=0x80              END=0xFF
	
	with all 256 bytes
	*/
	if((args[1][0] =='-') && (args[1][1] =='c') && (args[1][2] =='s') ){
		FILE *pfile2;
		uint32_t mstart;
		uint32_t mend;
		uint16_t numbgpr;
		uint32_t mstart1;
		uint32_t mend1;
		uint16_t numbgpr1;
	
		pfile2 = fopen ( args[2] ,"r" );
		if(pfile2==NULL) {
			fprintf( stderr, "bad open: %s\n" , args[2] );
			return 2;
		}
		
		mstart = 0;
		mend = 0;
		numbgpr = 0;
		mstart1 = 0;
		mend1 = 0;
		numbgpr1 = 0;
		
		while(1){
			if( 0 > getline ( &linetmp , &linesiz, pfile2 ) ) break;
			pstru = getnwathever (name, linetmp, sizeof(name) );
			/* fprintf( stderr, "%s\n" , name); */
			if ( strcmp( "DATABANK", name ) ) continue;
			
			pstru = getsymnwathever (name, pstru, sizeof(name) );
			/* fprintf( stderr, "%s\n" , name); */
			if ( strcmp( "NAME", name ) ) continue;
			
			pstru = getsymnwathever (name, pstru, sizeof(name) );
			/* fprintf( stderr, "%s\n" , name); */
			if ( strncmp( "gpr", name, 3 ) ) continue;
			if( name[3] < '0' || name[3] > '9' ) continue;
			/*pstru = getnumnwathever (&numbgpr, pstru, sizeof(numbgpr) );
			if(pstru==NULL) continue;
			fprintf( stderr, "gpr %u\n" , numbgpr);
			*/
			pstru = getsymnwathever (what, pstru, sizeof(what) );
			/* fprintf( stderr, "%s\n" , what); */
			if ( strcmp( "START", what ) ) continue;
			pstru = getnumnwathever (&mstart, pstru, sizeof(mstart) );
			/* fprintf( stderr, "0x%04x\n" , mstart); */
			if(pstru==NULL) continue;
			
			pstru = getsymnwathever (what, pstru, sizeof(what) );
			/* fprintf( stderr, "%s\n" , what); */
			if ( strcmp( "END", what ) ) continue;
			pstru = getnumnwathever (&mend, pstru, sizeof(mend) );
			/* fprintf( stderr, "0x%04x\n" , mend); */
			if(pstru==NULL) continue;
			
			if(mstart<mend) mend -= mstart;
			else continue;
			
			pstru = getnumnwathever (&numbgpr, name+3, sizeof(numbgpr) );
			
			if ( mend >= mend1 ) {
				mstart1 = mstart;
				mend1 = mend;
				numbgpr1 = numbgpr;
			}
			
			/* length end-start+1 */
		}
		
		fprintf( stderr, "pictc: stack start %4u siz %4u end %4u numbgpr %2u\n" , \
			mstart1, mend1+1, mstart1 + mend1, numbgpr1 );
		
	}
	
	
	
	
	return 0;
}

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

int readarg ( int argc, char *argv[] ) {

	int8_t i = 0;

	for ( i=1; i < argc - 1; ++i ) {
	 
		if ( strcmp ( argv[i], "-d"  ) == 0 ) {
			if(NULL==fndasm) fndasm = malloc(256);
			readstr (fndasm, argv[i+1], 256 );
			i += 1;
			continue;
		}
		
		if ( strcmp ( argv[i], "-m"  ) == 0 ) {
			if(NULL==fnmap) fnmap = malloc(256);
			readstr (fnmap, argv[i+1], 256 );
			i += 1;
			continue;
		}

	}

	return 0;
}

/* ---------------------------------------------------------- */
/*            end file                                        */
/* ---------------------------------------------------------- */
