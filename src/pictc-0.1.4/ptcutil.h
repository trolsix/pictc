/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/* ---------------------------------------------------------- */
/*                                                            */
/*                                                            */
/*                                                            */
/* ---------------------------------------------------------- */

#ifndef _PTCUTIL_H
#define _PTCUTIL_H      000104

unsigned char hashexdigit ( unsigned char tmp2 );
int readparam ( int argc, char * argv[] );
int readstr ( char * a, char * b, size_t s );
int szukaju ( char * a, char * b );
uint16_t getwathever (char *gdzie, char *co );
char * getnwathever (char *gdzie, void *co, int max );
char * getsymnwathever (char *gdzie, char *co, int max );
char * getnumnwathever ( void *, char * co, int max );

void setupper ( char * dst, char * src );

#endif
