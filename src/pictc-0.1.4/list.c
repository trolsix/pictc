/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/* ---------------------------------------------------------- */
/*                                                            */
/*      list                                                  */
/*                                                            */
/* ---------------------------------------------------------- */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ptcutil.h"

#define MAXFUN 1024

#define ddd 0

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

enum { nothing, bad, listnew, nameadd,
	nexist, nnoexist,	nameprint, listclr, namecut };

struct listg {
	char * name;
	uint32_t fgc ;
	uint32_t sizfun;
};

//uint8_t addglf ( struct listg * , char * , uint8_t );

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

uint8_t addglf ( struct listg * list, char * name , uint8_t action ) {

	char *listfun, *wsl;
	uint32_t fgc;
	uint32_t sizfun;
	uint32_t i;
	
	if(action==listnew) {
		list->name = malloc(256*MAXFUN);
		if(list->name==NULL) return bad;
		list->fgc = 0;
		list->sizfun = MAXFUN;
		return listnew;
	}
	
	fgc = list->fgc;
	sizfun = list->sizfun;
	listfun = list->name;
	
	wsl = listfun;
	
	if(ddd) fprintf( stderr, " lact %u ", action );
	
	if(action==nameadd) {
		
		if(ddd) fprintf( stderr, " %u" , fgc );
		
		for(i=0;i<fgc;++i) {
			if ( 0 == strncmp( name, wsl, 256 ) ) {
				if(ddd) fprintf( stderr, "ex %u\n" , i );
				return nexist;
			}
			wsl += 256;
		}
		
		if(ddd)fprintf( stderr, " l %p ", wsl );
		listfun  = wsl;
		
		if( fgc >= sizfun ) {
			char * tmp;
			tmp = realloc(list->name, 256*(list->sizfun + MAXFUN ));
			if(ddd) fprintf( stderr, "reall" );
			if(NULL!=tmp) {
				if(ddd) fprintf( stderr, " OK \n" );
				list->name = tmp;
				list->sizfun += MAXFUN;
				listfun = list->name;
				listfun += 256*fgc;
			}
			else {
				if(ddd) fprintf( stderr, " bad\n" );
				return bad;
			}
		}
		
		//listfun = list->name + (256*i);
		
		/* add into the list */
		readstr ( listfun, name, 256 );
		++list->fgc;
		if(ddd)fprintf( stderr, "el %u\n", list->fgc );
		return nameadd;
	}
	
	if(action==nexist) {
		if(ddd)fprintf( stderr, " ex \n" );
		for(i=0;i<fgc;++i) {
			if ( 0 == strncmp( name, wsl, 256 ) ) {
				return nexist;
			}
			wsl += 256;
		}
		return nnoexist;
	}
	
	if( action == nameprint ) {
		
		for( i=0; i<fgc; ++i ) {
			fprintf( stderr, "l: %s\n" , wsl );
			wsl += 256;
		}
		return nothing;
	}
	
	if( action == listclr ) {
		if(ddd)fprintf( stderr, " clr lis\n" );
		list->fgc = 0;
		return nothing;
	}
	
	/* remve everything after find this */
	if( action == namecut ) {
		
		for(i=0;i<fgc;++i) {
			if ( 0 == strncmp( name, wsl, 256 ) ) {
				if(ddd)fprintf( stderr, "cut %u\n" , i );
				break;
			}
			wsl += 256;
		}
		if(i>=fgc) return nothing;//not find
		list->fgc = i;
	}
	
	
	return nothing;
}

/* ---------------------------------------------------------- */
/*            end file                                        */
/* ---------------------------------------------------------- */
