/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */
/*  make automatic stack for pic18 read from linker script */
/*
find max DATABANK and read END
DATABANK   NAME=gpr0       START=0x80              END=0xFF

with all 256 bytes
*/
	
/* ---------------------------------------------------------- */

#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "ptcutil.h"

/* ---------------------------------------------------------- */

extern char *flink;
extern char *linetmp;
extern size_t linesiz;
extern uint8_t debufcount;

char *pstru;

#define lenghname 256

/* ---------------------------------------------------------- */

uint8_t readlinkfile ( void ) {

	FILE *pfile2;
	uint32_t mstart;
	uint32_t mend;
	uint16_t numbgpr;
	uint32_t mstart1;
	uint32_t mend1;
	uint16_t numbgpr1;
	char *name;
	char *what;

	pfile2 = NULL;
		
	if(flink) pfile2 = fopen ( flink ,"r" );
	else return 1;
	
	if(pfile2==NULL) {
		PRINT_INFO_ARG( 0, "bad open: %s\n" , flink );
		return 2;
	}
	
	PRINT_INFO_ARG( 0, "open %s\n" , flink );
	
	MALLOCRET(lenghname,name);
	MALLOCRET(lenghname,what);
	
	mstart = 0;
	mend = 0;
	numbgpr = 0;
	mstart1 = 0;
	mend1 = 0;
	numbgpr1 = 0;
	
	while(1){
		
		if( 0 > getline ( &linetmp , &linesiz, pfile2 ) ) break;

		pstru = getnwathever (name, linetmp, lenghname );
		/* fprintf( stderr, "%s\n" , name); */
		if ( strcmp( "DATABANK", name ) ) continue;
		
		pstru = getsymnwathever (name, pstru, lenghname );
		/* fprintf( stderr, "%s\n" , name); */
		if ( strcmp( "NAME", name ) ) continue;
		
		pstru = getsymnwathever (name, pstru, lenghname );
		/* fprintf( stderr, "%s\n" , name); */
		if ( strncmp( "gpr", name, 3 ) ) continue;
		if( name[3] < '0' || name[3] > '9' ) continue;
		/*pstru = getnumnwathever (&numbgpr, pstru, sizeof(numbgpr) );
		if(pstru==NULL) continue;
		fprintf( stderr, "gpr %u\n" , numbgpr);
		*/
		
		pstru = getsymnwathever (what, pstru, lenghname );
		/* fprintf( stderr, "%s\n" , what); */
		if ( strcmp( "START", what ) ) continue;
		pstru = getnumnwathever (&mstart, pstru, sizeof(mstart1) );
		/* fprintf( stderr, "0x%04x\n" , mstart); */
		if(pstru==NULL) continue;
	
		pstru = getsymnwathever (what, pstru, lenghname );
		/* fprintf( stderr, "%s\n" , what); */
		if ( strcmp( "END", what ) ) continue;
		pstru = getnumnwathever (&mend, pstru, sizeof(mend) );
		/* fprintf( stderr, "0x%04x\n" , mend); */
		if(pstru==NULL) continue;
		
		if(mstart<mend) mend -= mstart;
		else continue;
		
		pstru = getnumnwathever (&numbgpr, name+3, sizeof(numbgpr) );
		
		if ( mend >= mend1 ) {
			mstart1 = mstart;
			mend1 = mend;
			numbgpr1 = numbgpr;
		}
		
		/* length end-start+1 */
	}
	
	fprintf( stdout, "pictc: stack start %4u siz %4u end %4u numbgpr %2u\n" , \
		mstart1, mend1+1, mstart1 + mend1, numbgpr1 );
	
	free(name);
	free(what);
	
	return 0;
}

/* ---------------------------------------------------------- */
