/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018-2020 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "ptcutil.h"
#include "list.h"

/*-----------------------------------------------*/

extern uint8_t debufcount;

extern char *fndasm;
extern char *fnmap;
char *fld;

extern char *linetmp;
extern size_t linesiz;

char *name;

#define lenghtname 128

/*-----------------------------------------------*/

void savevalue( char );
void zerv(void);
void zerval(void);

static uint8_t gfopen(void);
uint8_t resmem(void);

/*-----------------------------------------------*/

static FILE * filedasm;
static FILE * fileldf;
static char * pstru;

/*-----------------------------------------------*/

uint32_t value;
uint16_t resram, resstack, ressum, stackmaxsave, stckbefore, nri;
int16_t numberdec, numberdecmax, numberdecloc;
uint8_t valw, rstckhard;

/*-----------------------------------------------*/

uint8_t islabel( char * str ) {
  while(*str){
    if(*str == ':') {
      *str = 0;
      return 1;
    }
    ++str;
  }
  return 0;
}

/*-----------------------------------------------*/

enum { FLABEL, FSETADR, FPOSTDEC, FPOSTINC, FJUMPLABEL, FBRANCHCOND };

static uint32_t flag;

/*-----------------------------------------------*/

char asmarg1[32];
char asmarg2[32];
char asmarg3[32];
char asmarg4[32];
char afterupper[32];

/*-----------------------------------------------*/

uint8_t getvalcommon ( char * str ) {
  
  uint8_t v;
  
  str = getlastfind ( str, "Common_RAM" );
  if ( ( str ) && ( getnumnwathever ( &v, str, 1 ) ) ) return v;
  
  return 0;
}

/*-----------------------------------------------*/

void checkwhatsup( void ){
  
  if( !asmarg2[0] ) return;
  
  setupper ( afterupper, asmarg2 );
  
  /* if return is after goto bra without btfss */
  /* then save this like e - because is not branch */
  if ( 0 == strcmp( "RETURN", afterupper ) ) {
    PRINT_INFO_ARG( 2, "find return %s\n" , asmarg3 );
    if ( FLAGIS(flag,FJUMPLABEL) ) {
      savevalue('e');
    } else
      savevalue('r');
    FLAGSET(flag,FJUMPLABEL);/* maybe not necessary */
    return;
  }
  
  if ( 0 == strcmp( "RETFIE", afterupper ) ) {
    PRINT_INFO_ARG( 2, "find retfie %s\n" , asmarg3 );
    savevalue('r');
    FLAGSET(flag,FJUMPLABEL);/* maybe not necessary */
    return;
  }
  
  FLAGCLR(flag,FJUMPLABEL);

  if ( 0 == strcmp( "GOTO", afterupper ) ) {
    PRINT_INFO_ARG( 2, "find goto %s\n" , asmarg3 );
    if( FLAGIS (flag,FBRANCHCOND) ) {
      savevalue('b');
      FLAGCLR(flag,FBRANCHCOND);
    } else {
      savevalue('g');
      FLAGSET(flag,FJUMPLABEL);
    }
    return;
  }
  
  if ( 0 == strcmp( "BRA", afterupper ) ) {
    PRINT_INFO_ARG( 2, "find bra goto %s\n" , asmarg3 );
    if( FLAGIS (flag,FBRANCHCOND) ) {
      savevalue('b');
      FLAGCLR(flag,FBRANCHCOND);
    } else {
      savevalue('g');
      FLAGSET(flag,FJUMPLABEL);
    }
    return;
  }
  
  FLAGCLR(flag,FBRANCHCOND);
    
  if ( 0 == strcmp( "MOVLW", afterupper ) ) {
    pstru = getnumnwathever (&valw, asmarg3, sizeof(valw) );
    PRINT_INFO_ARG( 2, "find movlw %u\n", valw  );
    return;
  }
  
  if ( 0 == strcmp( "ADDWF", afterupper ) ) {
    PRINT_INFO_ARG( 2, "find fsr1 %s\n" , asmarg2 );
    /* is modyfing fsr after call */
    setupper ( afterupper, asmarg3 );
    if ( 0 == strcmp( "FSR1L", afterupper ) ) {
      if(valw<=numberdec) numberdec -= valw;
    }
    return;
  }
  
  if ( 0 == strcmp( "SUBWF", afterupper ) ) {
    PRINT_INFO_ARG( 2, "find fsr1 %s" , asmarg2 );
    PRINT_INFO_ARG( 2, " %u\n", valw );
    /* is modyfing fsr after call */
    setupper ( afterupper, asmarg3 );
    if ( 0 == strcmp( "FSR1L", afterupper ) ) {
      numberdec += valw;
      if(numberdecmax<numberdec) numberdecmax = numberdec;
    }
    return;
  }

  if( ( 0 == strcmp( "RCALL", afterupper ) ) ||
    ( 0 == strcmp( "CALL", afterupper ) ) ) {
    PRINT_INFO_ARG( 2, "find call %s\n" , asmarg3 );
    savevalue('c');
    return;
  }
  
  /* if after is jump its branch */
  if(( 0 == strcmp( "BTFSS", afterupper ) ) ||
    ( 0 == strcmp( "BTFSC", afterupper ) ) ||
    ( 0 == strcmp( "DCFSNZ", afterupper ) ) ||
    ( 0 == strcmp( "DECFSZ", afterupper ) ) ||
    ( 0 == strcmp( "TSTFSZ", afterupper ) ) ||
    ( 0 == strcmp( "CPFSEQ", afterupper ) ) ||
    ( 0 == strcmp( "CPFSGT", afterupper ) ) ||
    ( 0 == strcmp( "CPFSLT", afterupper ) ) ||
    ( 0 == strcmp( "INFSNZ", afterupper ) ) ||
    ( 0 == strcmp( "INCFSZ", afterupper ) ) ) {
    PRINT_INFO_ARG( 2, "find branch cond %s\n" , asmarg3 );
    FLAGSET(flag,FBRANCHCOND);
    return;
  }
  
  /* common ram */
  if ( 0 == strcmp( "MOVFF", afterupper ) ) {
    uint8_t valcommon;
    PRINT_INFO( 2, "find movff\n" );
    setupper ( afterupper, asmarg4 );
    if ( 0 == strcmp( "POSTDEC1", afterupper ) ) {
      valcommon = getvalcommon (asmarg3);
      PRINT_INFO_ARG( 2, "find movff postdec %s", asmarg3 );
      PRINT_INFO_ARG( 2, " %u\n", valcommon );
      if(valcommon>resram) resram = valcommon;
    }
  }
  
  if ( 0 == strcmp( "PUSH", afterupper ) ) {
    PRINT_INFO_ARG( 2, "find push %s\n" , asmarg3 );
    savevalue('p');
    return;
  }
  
  /* test for other jump with label */
  setupper ( afterupper, asmarg3 );
  if ( 0 == strncmp( "LABEL_", afterupper, 6 ) ) {
    PRINT_INFO_ARG( 2, "find jump label %s\n" , asmarg3 );
    savevalue('b');
    /* FLAGSET(flag,FJUMPLABEL); */
    return;
  }
  
  setupper ( afterupper, asmarg3 );
  if ( 0 == strcmp( "POSTINC1", afterupper ) ) FLAGSET(flag,FPOSTINC);
  if ( 0 == strcmp( "PREINC1", afterupper ) ) FLAGSET(flag,FPOSTINC);
  if ( 0 == strcmp( "POSTDEC1", afterupper ) ) FLAGSET(flag,FPOSTDEC);
  
  setupper ( afterupper, asmarg4 );
  if ( 0 == strcmp( "POSTINC1", afterupper ) ) FLAGSET(flag,FPOSTINC);
  if ( 0 == strcmp( "PREINC1", afterupper ) ) FLAGSET(flag,FPOSTINC);
  if ( 0 == strcmp( "POSTDEC1", afterupper ) ) FLAGSET(flag,FPOSTDEC);
  
  if( FLAGIS(flag,FPOSTDEC) ) {
    FLAGCLR(flag,FPOSTDEC);
    PRINT_INFO_ARG( 2, "find postdec %i\n" , numberdec );
    ++numberdec;
  }
  
  if( FLAGIS(flag,FPOSTINC) ) {
    FLAGCLR(flag,FPOSTINC);
    PRINT_INFO_ARG( 2, "find postinc %i\n" , numberdec );
    --numberdec;
  }
  
  if(numberdecmax<numberdec) numberdecmax = numberdec;
  if ( stackmaxsave < numberdec ) stackmaxsave = numberdec;
}

/*-----------------------------------------------*/

unsigned int lookpicdasm ( char ** args ) {

  PRINT_INFO_ARG( 0, "lpdasm %s\n", fndasm );

  if( args == NULL ) return 1;
  if( resmem() ) return 1;
  if( gfopen() ) return 1;

  zerval();

  while( 0 < getline ( &linetmp , &linesiz, filedasm ) ) {
    
    PRINT_INFO_ARG( 3, "_A_ %s" , linetmp );
    
    pstru = getnwathever (name, linetmp, lenghtname );
    if( pstru == NULL ) continue;
    
    asmarg1[0] = 0;
    asmarg2[0] = 0;
    asmarg3[0] = 0;
    asmarg4[0] = 0;

    pstru = getnwathever( asmarg1, pstru, sizeof(asmarg1) );
    pstru = getnwathever( asmarg2, pstru, sizeof(asmarg2) );
    pstru = getargasm( asmarg3, pstru, sizeof(asmarg3) );
    pstru = getnwathever( asmarg4, pstru, sizeof(asmarg4) );
    
    if( debufcount>3 ){
      fprintf( stderr, "_p %s" , name );
      fprintf( stderr, ",%s" , asmarg1 );
      fprintf( stderr, ",%s" , asmarg2 );
      fprintf( stderr, ",%s" , asmarg3 );
      fprintf( stderr, ",%s\n" , asmarg4 );
    }

    
    if( name[0] == ';' ) {
      continue;
    }
    
    /* test for max stack when decrasees and no label itp */
    
    /* vector reset is like function */
    if(( ( name[0] == 'v' ) && ( name[1] == 'e' ) ) ||
      ( ( name[0] == 'f' ) && ( name[1] == 'u' ) ) ) {
      
      PRINT_INFO_ARG( 2, "_f_ %s\n" , name );
      
      /* really label ? */
      if( !islabel( name ) ) continue;

      if( FLAGIS(flag,FSETADR) ) {
        PRINT_INFO_ARG( 0, "bad parse file: %s\n" , fndasm );
        return 1;
      }
        
      if( FLAGIS(flag,FJUMPLABEL) ) {
        if( FLAGIS(flag,FLABEL) ) { /* summary for end */
          savevalue('e');
          zerv();
          PRINT_INFO( 2, "_e_ " );
        }
      }
      
      FLAGSET(flag,FLABEL);
      FLAGSET(flag,FSETADR);
      savevalue('F');
      PRINT_INFO_ARG( 2, "_f_ %s\n", name );
    }
    
    
    /* labele */
    if( name[0] == 'l' ) {
      
      if( !islabel( name ) ) continue;
      
      /* for continue asm code small l */
      if( FLAGIS(flag,FJUMPLABEL) ) {
        if( FLAGIS(flag,FLABEL) ) { /* summary for end */
          savevalue('e');
          zerv();
          PRINT_INFO( 2, "_e_ " );
        }
        PRINT_INFO_ARG( 2, "L %s", name );
        FLAGSET(flag,FSETADR);
        savevalue('L');
      } else {
        savevalue('l');
      }
      
      FLAGSET(flag,FLABEL);
      PRINT_INFO_ARG( 2, "_L_ %s\n", name );
    }
    
    
    /* if get adres */
    if( getnumbhex ( &value, name, sizeof(value) ) ) {
      
      PRINT_INFO_ARG( 2, "_adr %s" , name );
      
      if( FLAGIS(flag,FSETADR) ) {
        FLAGCLR(flag,FSETADR);
        fprintf ( fileldf, " %u\n", value );
      }
      
      /* this code for number without label
      if( !FLAGIS(flag,FLABEL) ) {
        if( !islabel( name ) ) continue;
        fprintf ( fileldf, "l %s\n", name );
        fprintf ( stderr, "_l_ %s\n", name );
      }*/
      
      PRINT_INFO_ARG( 2, "_adr3 %s" , name );
      
      checkwhatsup();
    }
    
  }

  savevalue('e');
  zerv();
  
  fclose( filedasm );
  fflush( fileldf );
  fclose( fileldf );
  free( name );
  
  PRINT_INFO( 2, "\n");
  
  return 0;
}

/*-----------------------------------------------*/

static uint8_t gfopen(void){

  filedasm = fopen ( fndasm, "r" );
  if( filedasm == NULL ) return 1;

  fileldf = fopen ( "_lisdfun.txt", "w" );
  if( fileldf == NULL ) {
    PRINT_INFO( 0, "not open _lisdfun.txt\n" );
    return 1;
  }
  
  PRINT_INFO( 0, "open _lisdfun.txt\n" );
  
  return 0;
}


/*-----------------------------------------------*/

uint8_t resmem(void) {
  
  name = malloc(lenghtname);
  if( name == NULL ) return 1;
  
  return 0;
}

/*-----------------------------------------------*/

void savevalue( char ident ) {

  if( ( stackmaxsave > numberdec ) && ( stackmaxsave > stckbefore ) ) {
    fprintf ( fileldf, "i _*%d*_", nri++ );
    fprintf ( fileldf, " %i" , stackmaxsave );
    fprintf ( fileldf, " %u" , resram );
    /* fprintf ( fileldf, " %u" , resstack ); */
    fprintf ( fileldf, " %u" , numberdecmax );
    fprintf ( fileldf, "\n" );
  }
  
  /* for label and difer stack save i */
  /* except before was e or r */
  if( (ident == 'l') || (ident == 'L') || (ident == 'F') ) {
    if( stckbefore != numberdec ) {
      fprintf ( fileldf, "i _*%d*_", nri++ );
      fprintf ( fileldf, " %i" , numberdec );
      fprintf ( fileldf, " %u" , resram );
      /* fprintf ( fileldf, " %u" , resstack ); */
      fprintf ( fileldf, " %u" , numberdecmax );
      fprintf ( fileldf, "\n" );
    }
  }
  
  stckbefore = numberdec;
  stackmaxsave = numberdec;
  
  fprintf ( fileldf, "%c" , ident );
  
  if((ident == 'c')||(ident == 'g')||(ident == 'b'))
    fprintf ( fileldf, " %s", asmarg3 );
  
  if((ident == 'l')||(ident == 'L')||(ident == 'F'))
    fprintf ( fileldf, " %s", name );
  
  /* fprintf ( fileldf, " %u" , resstack ); */
  fprintf ( fileldf, " %i" , numberdec );
  fprintf ( fileldf, " %u" , resram );
  fprintf ( fileldf, " %u" , numberdecmax );
  
  if( !FLAGIS(flag,FSETADR) )
    fprintf ( fileldf, "\n" );  
}

/*-----------------------------------------------*/

void zerv( void ){
  
  stackmaxsave = 0;
  resram = 0;
  value = 0;
  resstack = 0;
  numberdec = 0;
  numberdecmax = 0;
}

/*-----------------------------------------------*/

void zerval( void ){

  stckbefore = 0;
  stackmaxsave = 0;
  rstckhard = 0;
  value = 0;
  resram = 0;
  resstack = 0;
  ressum = 0;
  numberdec = 0;
  numberdecmax = 0;
  numberdecloc = 0;
  valw = 0;
  flag = 0;
  nri = 0;
}

/*-----------------------------------------------*/
