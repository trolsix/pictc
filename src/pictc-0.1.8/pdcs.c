/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018-2020 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/*-----------------------------------------------*/
/*                                               */
/*    read _lisdfun.txt                          */
/*                                               */
/*-----------------------------------------------*/

#include <stdio.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "ptcutil.h"
#include "list.h"

/*-----------------------------------------------*/

#define STEPMODE 0

/*-----------------------------------------------*/

extern uint8_t debufcount;
extern char *fnmap;

/*-----------------------------------------------*/

uint8_t isonthelist ( uint32_t *list, uint32_t what, uint32_t max );
void zerval(void);

/*-----------------------------------------------*/

extern uint32_t value;
extern uint16_t resram, resstack, ressum;
extern int16_t numberdec, numberdecmax, numberdecmaxall, numberdecloc;
extern uint8_t valw, rstckhard;

extern char *linetmp;
extern size_t linesiz;

extern char asmarg1[32];
extern char asmarg2[32];
extern char asmarg3[32];
extern char asmarg4[32];
extern char afterupper[32];

struct fun {
  char name[32];
  char what;
  int16_t stack;
  uint8_t regist;
};

/*-----------------------------------------------*/

static FILE * fileldf;
static FILE * filem;

static uint16_t funnumb, itemsnumb, labnumb;
static struct fun * funwsk, *faw;
  
/*-----------------------------------------------*/

static uint8_t gfopen(void);
static void * readdata ( void );
static void readitem ( void );
static int32_t getnrlabel( uint32_t );
static void flowback ( void );
static void printsummary ( void );

/*-----------------------------------------------*/

static uint32_t *listpath, *listjump, *listlab, *listcall;
static uint32_t nrpath, nrfcall;

static uint32_t nrwork, nrljump, nrlab;
static uint8_t nrloop;
static uint8_t checkalljump;

static uint32_t cnrfb;

/*-----------------------------------------------*/

struct funsumary {
  uint32_t nronfaw;   /* nr on the list */
  uint32_t adr;       /* adres in flash */
  int16_t stckrfun;   /* for return funkcion */
  int16_t stckxfun;   /* for return funkcion */
  int16_t stckxall;   /* max sum */
  uint8_t stckhard;   /* sum of call */
  uint8_t done;       /* was done */
  uint8_t registf;    /* reg for fun  */
  uint8_t regista;    /* max reg */
};

static struct funsumary * funsum;

/*-----------------------------------------------*/

static int32_t getnrlabel( uint32_t l ) {
  
  uint32_t i;
  char * n, w;
  struct fun * f;
  
  f = funwsk;
  n = f[l].name;
  
  for( i=0; i<itemsnumb; ++i, ++f ) {
    w = f->what;
    if(('L'==w)||('l'==w)||('F'==w))
      if ( 0 == strcmp( f->name, n ) )
        return i;
  }
  
  /* should never happen */
  if(debufcount)
    fprintf( stderr, "don't find label: %s on the list\n", n );
  
  exit(1);
}

/*-----------------------------------------------*/

static uint32_t findnrfun ( uint32_t nr ) {
  
  uint16_t i;
  struct funsumary * fs;
  
  for ( fs=funsum, i=0; i<funnumb; ++i, ++fs )
    if( fs->nronfaw == nr ) return i;
  
  return 0x80000000;
}

/*-----------------------------------------------*/

static uint32_t findadrfun ( uint32_t adr ) {
  
  struct funsumary * fs;
  uint16_t i;
  
  fs = funsum;
  
  for ( i=0; i<funnumb; ++i, ++fs ) {
    if( fs->adr == adr ) return i;
  }
  
  return 0x80000000;
}

/*-----------------------------------------------*/

static uint32_t getnrinlist ( uint32_t *list, uint32_t what, uint32_t max ) {
  
  while(max--) {
    if( what == list[max] ) return max;
  }

  return 0;
}

/*-----------------------------------------------*/
/*

1. find all function and read max register
calculate:
- soft stack usage for fun
- max soft stack usage for fun
- max soft stack usage for fun with call
- max hard stack usage for fun with call

- calculate max for not return funkcion

- if read map files print original name

*/

uint8_t dcs ( void ) {
  
  uint32_t ms;
  
  checkalljump = 0;
  cnrfb = 0;
  
  if( gfopen() ) return 1;
  
  PRINT_INFO( 1, "dcs start\n" );
  
  /* count number items */
  readitem();
  
  MALLOCRET(itemsnumb,funwsk);
  MALLOCRET(itemsnumb,listpath);
  MALLOCRET(labnumb,listjump);
  MALLOCRET(labnumb,listlab);
  MALLOCRET(funnumb,funsum);
  MALLOCRET(funnumb,listcall);

  /* read data */
  funwsk = readdata();
  if( NULL == funwsk ) return 2;

  /* mem summary */
  ms = itemsnumb * sizeof(*funwsk);
  ms += itemsnumb*sizeof(*listpath);
  ms += labnumb*sizeof(*listjump);
  ms += labnumb*sizeof(*listlab);
  ms += funnumb*sizeof(*funsum);
  ms += funnumb*sizeof(*listcall);
  
  PRINT_INFO_ARG( 1, "alloc %u bytes\n" , ms );
  
  nrloop = 1;

  LOOP_SUM:

  fseek( fileldf, 0, SEEK_SET );
  
  nrlab = 0;
  nrljump = 0;
  nrpath = 0;
  nrwork = 0;
  nrfcall = 0;

  for( ; nrwork<itemsnumb; ) {
    
    faw = funwsk + nrwork;

    if(debufcount>2) {
      fprintf( stderr, "%4u" , nrpath );
      fprintf( stderr, " work %4u" , nrwork );
      fprintf( stderr, " lab %3u" , nrlab );
      fprintf( stderr, " %c" , faw->what );
      fprintf( stderr, " %3i" , faw->stack );
      fprintf( stderr, " %s" , faw->name );
    }
    if( ( nrpath == 0  ) && ( faw->what != 'F' ) ) {
      ++nrwork;
      PRINT_INFO( 2, "\n" );
      continue;
    }

    /* find new */
    if( faw->what=='F') {
      uint32_t fn;
      
      if((nrloop == 0)&&(debufcount>2)) {
        fprintf( stderr, "-----------------------------\n" );
        fprintf( stderr, "fun %u work: %u" , nrpath, nrwork );
        fprintf( stderr, " %c %s\n" , faw->what, faw->name );
      }
      /* if was made skip */
      fn = findnrfun(nrwork);
      if ( funsum[fn].done & 0x01 ) {
        ++nrwork;
        continue;
      }
      
      listpath[nrpath++] = nrwork++;
      PRINT_INFO( 2, "\n" );
      continue;
    }
    
    /* find goto or branch */
    if( ( faw->what=='g') || ( faw->what=='b') ){
      
      int32_t nritem = getnrlabel( nrwork );
      
      listpath[nrpath] = nrwork;
      
      if( !checkalljump ){
        if ( isonthelist( listlab, nritem, nrlab ) ) {
          ++nrwork;
          ++nrpath;
          PRINT_INFO( 2, " is in listlab\n" );
          continue;
        }
      }

      if( isonthelist ( listpath, nritem, nrpath ) ) {
        ++nrwork;
        PRINT_INFO( 2, " is in list\n" );
      } else {
        listjump[nrljump++] = nrpath;
        nrwork = nritem;
        PRINT_INFO_ARG( 2, " jump %u\n", nrwork );
      }
      ++nrpath;
      continue;
    }
    
    /* find label */
    if( ( faw->what=='l') || ( faw->what=='L') ) {
      
      if ( !isonthelist( listlab, nrwork, nrlab ) )
        listlab[nrlab++] = nrwork;
      listpath[nrpath++] = nrwork++;
      
      PRINT_INFO( 2, "\n" );
      continue;
    }
    
    if( faw->what=='c') {
      uint32_t nritem, i;
      
      /* skip in first look */
      listpath[nrpath] = nrwork;
      
      if( nrloop == 0 ) {
        ++nrwork;
      } else {
        nritem = getnrlabel( nrwork );
        
        /* if exist means recursive and warn and calculate */
        /* or get the next element */
        if( isonthelist ( listpath, nritem, nrpath ) ) {
          PRINT_INFO_ARG( 1, "recursive %s\n", faw->name );
          flowback ();
          continue;
        }
        /* if funkcion was maked just skip */
        i = findnrfun ( nritem );
        
        /* if get stack calculate skip this call */
        if ( funsum[i].done & 0x01 ){
          ++nrwork;
          ++nrpath;
          PRINT_INFO( 2, " skip call\n" );
          continue;
        }
        
        PRINT_INFO_ARG( 2, " jump %u", nritem );
        PRINT_INFO_ARG( 2, " %u", nrpath );

        listjump[nrljump++] = nrpath;
        nrwork = nritem;
        listcall[nrfcall++] = nrwork;
      }
      
      ++nrpath;
      
      PRINT_INFO_ARG( 2, " j: %3u", nrljump );
      PRINT_INFO_ARG( 2, " c: %3u\n", nrfcall );
      continue;
    }
    
    /* find linfo */
    if ( ( faw->what=='i') || ( faw->what=='p') ){
      listpath[nrpath++] = nrwork++;
      PRINT_INFO( 2, "\n" );
      continue;
    }
    
    /* find end */
    if( faw->what=='r') {
      listpath[nrpath++] = nrwork++;
      PRINT_INFO( 2, "\n" );
      flowback ();
      continue;
    }
    
    /* find end */
    if( faw->what=='e') {
      listpath[nrpath++] = nrwork++;
      PRINT_INFO( 2, "\n" );
      flowback ();
      continue;
    }
    
    nrwork++;
    fprintf( stderr, "=\n" );
  }

  if ( ++nrloop < 2 ) {
    if(debufcount>2) {
      fprintf( stderr, "============ loop2 ==========\n" );
    }
    goto LOOP_SUM;
  }
  
  printsummary();
  
  if ( fileldf ) fclose(fileldf);
  if ( filem ) fclose(filem);
  
  free(funwsk);
  free(listpath);
  free(listjump);
  free(listlab);
  free(funsum);
  free(listcall);
  
  return 0;
}

/*-----------------------------------------------*/

static char *mname;
static uint32_t madr;

static uint8_t getentrycode ( void ) {
  
  char * wsl;
  
  MALLOCRET(128,mname);
  
  while ( 0 < getline ( &linetmp , &linesiz, filem ) ) {
    
    wsl = getnwathever ( mname, linetmp, 128 );
    wsl = getnwathever ( asmarg1, wsl, 32 );
    wsl = GETNUMB( madr, wsl );
    wsl = getnwathever ( asmarg2, wsl, 32 );
    
    if ( 0 != strcmp( "code", asmarg1 ) ) continue;
    if ( 0 != strcmp( "program", asmarg2 ) ) continue;
    
    return 0;
  }
  
  
  return 1;
}

/*-----------------------------------------------*/
/*
  uint32_t nronfaw;
  uint8_t registf;
  uint8_t regista;
*/
/*
S_file__fun       code   0x002a64    program   0x000154
_fun   0x002a64    program     static file.asm
*/

static void printsummary ( void ) {
  
  uint32_t nrf;
  struct fun * f;
  struct funsumary *fs;
  
  PRINT_INFO_ARG(1,"analize %u branch\n", cnrfb );
  
  fprintf( stdout, "\n" );
  fprintf( stdout, " %40s", "name" );
  fprintf( stdout, "      adr" );
  fprintf( stdout, "  stckf" );
  fprintf( stdout, " stcka" );
  fprintf( stdout, " stckh" );
  
  if ( filem ) {

    while ( 0 == getentrycode() ){
    
      /* find fun */
      nrf = findadrfun ( madr );
      if ( nrf >= funnumb ) continue;
      
      fs = funsum + nrf;
      
      fprintf( stdout, "\n" );
      fprintf( stdout, " %40s", mname );
      fprintf( stdout, " 0x%06x", fs->adr );
      fprintf( stdout, " %5u", fs->stckxfun );
      fprintf( stdout, " %5u", fs->stckxall );
      fprintf( stdout, " %5u", fs->stckhard );
      
      /* deactivate adr */
      fs->adr = 0x80000000;
      free(mname);
    }
    
  }
  
  for ( nrf=0, fs=funsum; nrf<funnumb; ++nrf, ++fs ) {
      
    f = funwsk + fs->nronfaw;
    if( 0x80000000&fs->adr ) continue;
    fprintf( stdout, "\n" );
    fprintf( stdout, " %40s", f->name );
    fprintf( stdout, " 0x%06x", fs->adr );
    fprintf( stdout, " %5u", fs->stckxfun );
    fprintf( stdout, " %5u", fs->stckxall );
    fprintf( stdout, " %5u", fs->stckhard );
  }
  
  fprintf( stdout, "\n" );
}

/*-----------------------------------------------*/
/* analyze from 0 -> nrwork */

static void stckcmp ( uint32_t belem ) {
  
  int16_t stmp1, stmp2;
  
  /* new */
  if ( (int16_t)0x8000 == funsum[belem].stckrfun ) {
    
    funsum[belem].stckrfun = numberdec;
    
    return;
  }

  /* compare */
  
  if( funsum[belem].stckrfun < 0 ) stmp1 = -funsum[belem].stckrfun;
  else stmp1 = funsum[belem].stckrfun;
  
  if( numberdec < 0 ) stmp2 = -numberdec;
  else stmp2 = numberdec;
  
  if( stmp2 != stmp1 ) {
    if(debufcount>0) {
      uint32_t nrf = funsum[belem].nronfaw;
      fprintf( stderr, " %s", funwsk[nrf].name );
      fprintf( stderr, " differ stack %i", funsum[belem].stckrfun );
      fprintf( stderr, " %i", numberdec );
    }
    if( stmp2 > stmp1 ) {
      if(debufcount>0)
        fprintf( stderr, " get: %i", numberdec );
      funsum[belem].stckrfun = numberdec;
    }
    if(debufcount>0)
      fprintf( stderr, "\n");
  }

}

/*-----------------------------------------------*/

static void flowstack ( void ) {
  
  struct fun * f;
  uint32_t elem, nrtw, belem;
  uint8_t numberhardstc;
  
  PRINT_INFO(3,"flow back welcome");
  
  /* if has no call means top funkcion */
  if( nrfcall == 0 ) {
    
    PRINT_INFO(3," nrfcall 0 top fun");
    
    elem = listpath[0];
    belem = findnrfun (elem);

    PRINT_INFO_ARG(3," %u", elem);
    PRINT_INFO_ARG(3," %s", funwsk[elem].name);
    
    elem = 0;
    /* if funkcion was calulated skip this */
  } else {
    elem = listcall[nrfcall-1];
    
    PRINT_INFO_ARG(3," %u", elem);
    PRINT_INFO_ARG(3," %s", funwsk[elem].name);
    
    belem = findnrfun ( elem );
    elem = getnrinlist ( listpath, elem, nrpath );
  }
  
  PRINT_INFO_ARG(3," w: %u", elem);
  PRINT_INFO_ARG(3," f: %u", belem);
  PRINT_INFO_ARG(3," %i\n", funsum[belem].stckxfun);
  
  numberdec = 0;
  numberdecmax = funsum[belem].stckxfun;
  numberhardstc = funsum[belem].stckhard;
  
  if( nrpath==0 ) exit(1);

  nrtw = listpath[elem];
  f = funwsk + nrtw;
  
  /* analyze fun branch but if 'r' dont update stckr */
  while( ++elem < nrpath ) {
    uint32_t fc;
    int16_t st;
    
    st = 0;
    nrtw = listpath[elem];
    f = funwsk + nrtw;
    numberdec += f->stack;
    
    if( numberdecmax < numberdec ) numberdecmax = numberdec;
    if( funsum[belem].stckxall < numberdecmax )
        funsum[belem].stckxall = numberdecmax;
    
    if( debufcount>3 ){
      fprintf( stderr, " %c", f->what );
      fprintf( stderr, " %3u.%3u.%3i", elem, nrtw, numberdec );
      fprintf( stderr, " %s", f->name );
    }
    
    /* stack for push */
    
    if( ( f->what == 'p' ) && ( 0 == numberhardstc ) )
      numberhardstc = 1;
    
    if( f->what == 'c' ) { /* read stack */
      
      fc = findnrfun ( getnrlabel( nrtw ) );
      
      /* hardware stack */
      if( (funsum[fc].stckhard+1) > numberhardstc )
        numberhardstc = funsum[fc].stckhard + 1;
      
      PRINT_INFO_ARG(3," hs %3u", funsum[fc].stckhard+1 );
      
      /* software stack */
      st = funsum[fc].stckxall;
      
      PRINT_INFO_ARG(3, " sa %3i", st  );
      
      if( funsum[belem].stckxall<(numberdec+st) )
        funsum[belem].stckxall = numberdec+st;
      
      st = funsum[fc].stckrfun;
      
      PRINT_INFO_ARG(3, " sr %3i", st  );
      
      if ( (int16_t) 0x8000 != st ) { /* new */
        numberdec += st;
      } else {
        PRINT_INFO( 3, " fun not return" );
      }
      if ( numberdecmax < numberdec ) numberdecmax = numberdec;
      if ( funsum[belem].stckxall < numberdecmax )
        funsum[belem].stckxall = numberdecmax;
    }
    
    PRINT_INFO(3, "\n" );
  }
  
  /* max witch call inside stckxall */
  funsum[belem].stckxfun = numberdecmax;
  funsum[belem].stckhard = numberhardstc;
  
  /* compare and write if has r */
  if( f->what == 'r' )
    stckcmp ( belem );

  
  if(debufcount>3) {
    
    fprintf( stderr, " stack" );
  
    if ( -0x8000 == funsum[belem].stckrfun )
      fprintf( stderr, " r --- " );
    else
      fprintf( stderr, " r %3i ", funsum[belem].stckrfun );

    fprintf( stderr, " x %3i ", funsum[belem].stckxfun );
    fprintf( stderr, " a %3i ", funsum[belem].stckxall );
    fprintf( stderr, " h %3i ", funsum[belem].stckhard );
  }
  
  
  /* return to the point jump */
  if( nrljump == 0 ) {
    funsum[belem].done |= 0x01;
    PRINT_INFO_ARG(3, " end: %s", funwsk[listpath[0]].name );
    PRINT_INFO(3, " ----\n");
    nrpath = 0;
    nrlab = 0;
    zerval();
    nrwork = listpath[nrpath];
    ++nrwork;
    #if STEPMODE==1
    fgetc(stdin);
    #endif
    return;
  }
  
  elem = listjump[--nrljump];

  PRINT_INFO_ARG(3, " elem jump %3u", elem );

  if(elem==0) {
    PRINT_INFO(0, "Bad nrpath = 0\n" );
    exit(1);
  }
  
  nrpath = elem;
  nrwork = listpath[nrpath];
  
  /* if jump is call remove call for listcall */
  f = funwsk + nrwork;
  if( f->what == 'c' ) {
    funsum[belem].done |= 0x01;
    PRINT_INFO(3, " done jump call" );
    if(nrfcall)--nrfcall; /* bad if this is 0 */
    PRINT_INFO_ARG(3, " %3u", listcall[nrfcall] );
  }

  ++nrwork;
  ++nrpath;

  /*
  if ( nrljump ) {
    ++nrwork;
    ++nrpath;
  } else {
    PRINT_INFO_ARG(0, " really non jump w %3u", nrwork );
    PRINT_INFO_ARG(0, " p %3u", nrpath );
    fgetc(stdin);
  }
  */
  
#if BLEBLE1 
  while(1) {
    --nrpath;
    if(nrpath == 0 ) {
      PRINT_INFO(0, "Bad nrpath = 0\n" );
      exit(1);
    }
    if( elem == nrpath ) {
      nrwork = listpath[nrpath];
      /* if jump is call remove call for listcall */
      f = funwsk + nrwork;
      if( f->what == 'c' ) {
        funsum[belem].done |= 0x01;
        PRINT_INFO(3, " done jump call" );
        if(nrfcall)--nrfcall; /* bad if this is 0 */
        PRINT_INFO_ARG(3, " %3u", listcall[nrfcall] );
      }
      if ( nrljump ) {
        ++nrwork;
        ++nrpath;
        break;
      }
      break;
    }
  }
#endif
  
  PRINT_INFO_ARG(3, " flow back bye bye %u", nrwork );
  PRINT_INFO_ARG(3, " %u\n", nrpath );

  return; 
}

/*-----------------------------------------------*/

static void flowback ( void ) {

  struct fun * f;
  uint32_t elem;
  
  /* find call funkcion on the list work */
  
  if( nrloop == 1 ) {
    ++cnrfb;
    flowstack ();
    return;
  }
  
  /* only for read register - not work yet  */
  /* beck until elemn is on the jump list  */

  if( nrljump == 0 ) elem = -1;
  else elem = listjump[--nrljump];
  
  PRINT_INFO_ARG( 2, " b elem %i \n" , elem );
  
  while(1){
    --nrpath;

    f = funwsk + listpath[nrpath];
    if ( f->regist > resram ) resram = f->regist;
    
    if( f->what == 'r' ) numberdec = 0;
    
    numberdec += f->stack;
    fprintf( stderr, "d %4u stck %3i %3i\n", nrpath, f->stack, numberdec );
    
    if(nrpath == 0 ) {
      fprintf( stderr, "regist: %u\n" , resram );
      fprintf( stderr, "stacks: %u\n" , numberdecmax );
      if(debufcount>2) {
        fprintf( stderr, "nrpath equal 0\n" );
        fprintf( stderr, "-----------------------------\n" );
      }
      nrlab = 0;
      zerval();
      nrwork = listpath[nrpath];
      ++nrwork;
      return;
    }
    
    if( elem == nrpath ) {
      nrwork = listpath[nrpath];
      if ( nrljump ) {
        ++nrwork;
        ++nrpath;
        break;
      }

    }
  }
  
  
  if(debufcount>2)
    fprintf( stderr, "flow back byby %u %u\n", nrwork, nrpath );
}

/*-----------------------------------------------*/

static void readitem ( void ) {

  funnumb = 0;
  itemsnumb = 0;
  labnumb = 0;
  
  while( 0 < getline ( &linetmp , &linesiz, fileldf ) ) {
    ++itemsnumb;
    if( linetmp[0] == 'F') ++funnumb;
    if( ( linetmp[0] == 'F') ||
      ( linetmp[0] == 'L') ||
      ( linetmp[0] == 'l')  ) ++labnumb;
  }
  
  PRINT_INFO_ARG( 1, "find %u items" , itemsnumb );
  PRINT_INFO_ARG( 1, " %u label\n" , labnumb );
}

/*-----------------------------------------------*/

static void * readdata ( void ){
  
  uint32_t nrinlist, nrfun;
  int16_t stckb[2];

  faw = funwsk;
  
  nrinlist = 0;
  nrfun = 0;
  stckb[0] = 0;
  stckb[1] = 0;
  
  fseek( fileldf, 0, SEEK_SET );
  
  while( 0 < getline ( &linetmp , &linesiz, fileldf ) ) {
    char chf, *wsl;
    
    chf = linetmp[0];
    faw->what = chf;
    
    wsl = &linetmp[2];
    
    faw->name[0] = 0;
    faw->stack = 0;
    faw->regist = 0;
    
    if( ( chf != 'e') && ( chf != 'r' ) && ( chf != 'p' ) )
      wsl = getnwathever ( faw->name, &linetmp[2], 32 );
    
    wsl = GETNUMB( faw->stack, wsl );
    wsl = GETNUMB( faw->regist, wsl );

    stckb[0] = faw->stack;
    
    /* if(nrinlist) faw->stack = stckb[1] - faw->stack; */
    if(nrinlist) faw->stack -= stckb[1];
    stckb[1] = stckb[0];
    
    if( ( chf == 'e') || ( chf == 'r' ) )
      stckb[1] = 0;
    
    /* if funkcion */
    if ( chf == 'F' ) {
      uint32_t adr = 0;
      funsum[nrfun].nronfaw = nrinlist;
      wsl = GETNUMB( adr, wsl );
      wsl = GETNUMB( adr, wsl );
      funsum[nrfun].adr = adr;
      funsum[nrfun].stckrfun = (int16_t)0x8000;
      funsum[nrfun].stckxfun = 0;
      funsum[nrfun].stckxall = 0;
      funsum[nrfun].stckhard = 0;
      funsum[nrfun].done = 0;
      
      PRINT_INFO_ARG( 2, "%16s" , faw->name );
      PRINT_INFO_ARG( 2, " %3u", nrfun );
      PRINT_INFO_ARG( 2, " %3u", nrinlist );
      PRINT_INFO_ARG( 2, " 0x%07x\n" , adr );
      
      ++nrfun;
    } else {
      /*
      PRINT_INFO_ARG( 2, "%16s" , faw->name );
      PRINT_INFO_ARG( 2, " %3u\n", nrinlist );
      */
    }
    
    ++faw;
    ++nrinlist;
  }
  
  return funwsk;
}

/*-----------------------------------------------*/

static uint8_t gfopen(void){

  if(fnmap) {
    filem = fopen ( fnmap, "r" );
    if ( ( filem == NULL ) && (debufcount) ) 
      fprintf( stderr, "bad open map file %s\n" , fnmap );
  } else  
    filem = NULL;

  if( filem && debufcount )
    fprintf( stderr, "open map file: %s\n" , fnmap );

  
  fileldf = fopen ( "_lisdfun.txt", "r" );
  
  if( fileldf == NULL ) {
    PRINT_INFO( 0, "bad open file _lisdfun.txt\n" );
    return 1;
  }
  
  if( fileldf && debufcount )
    fprintf( stderr, "open file: %s\n" , "_lisdfun.txt" );
  
  return 0;
}

/*-----------------------------------------------*/
/*                                               */
/*-----------------------------------------------*/
