/*-----------------------------------------------

author: Tomasz C.
trol.six www.elektroda.pl
aktyn www.gentoo.org

MIT License
Copyright (c) 2018-2020 trolsix
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
-----------------------------------------------*/
/* -----------------------------------------------

  asm analyzer

----------------------------------------------- */

#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "ptcutil.h"

/* ---------------------------------------------------------- */

const char pictcver[] = "pictc ver 0.1.8";

/* ---------------------------------------------------------- */

void showhelp ( void );
int readarg ( int argc, char *argv[] );

uint8_t lookpicasm ( char **args );
uint8_t cs ( void );
unsigned int lookpicdasm ( char ** );
uint8_t dcs ( void );
uint8_t readlinkfile ( void );

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

char *linetmp;
size_t linesiz = 1024;

uint8_t debufcount;

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

char *fndasm;
char *fnmap;
char *flink;

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

enum { version, help, mkasm, casm, mkdasm, cdsm, inflink };

static uint8_t mainwork;

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

int main(int argc, char *args[]) {
	
	int r, nrarg;

	fndasm = NULL;
	fnmap = NULL;
	flink = NULL;

	mainwork	= 0;
	debufcount = 1;
	
	if( argc == 0 ) return 0;

	nrarg = readarg ( argc, args );
	
	if( FLAGIS(mainwork,version) ) {
		fprintf( stdout, "%s\n" , pictcver );
	}
	if( FLAGIS(mainwork,help) ) {
		showhelp ();
		return 0;
	}
	
	linetmp = malloc(linesiz);
	
	if( FLAGIS(mainwork,mkasm) ) {
		r = lookpicasm ( args+nrarg );
		if(r) return r;
	}
	if( FLAGIS(mainwork,casm) ) {
		r = cs();
		if(r) return r;
	}
	if( FLAGIS(mainwork,inflink) ) {
		r = readlinkfile ();
		if(r) return r;
	}
	if( FLAGIS(mainwork,mkdasm) ) {
		r = lookpicdasm ( args );
		if(r) return r;
	}
	if( FLAGIS(mainwork,cdsm) ) {
		r = dcs ();
		if(r) return r;
	}

	return 0;
}

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

int readarg ( int argc, char *argv[] ) {

	int8_t i;

	for ( i=1; (i < argc) && (argv[i]); ++i ) {
		
		if ( strcmp( argv[i], "-d"  ) == 0 ) {
			if(NULL==fndasm) fndasm = malloc(256);
			readstr(fndasm, argv[++i], 256 );
			FLAGSET(mainwork,mkdasm);
			continue;
		}
		if ( strcmp( argv[i], "-h"  ) == 0 ) {
			FLAGSET(mainwork,help);
			continue;
		}
		if ( strcmp( argv[i], "--help"  ) == 0 ) {
			FLAGSET(mainwork,help);
			continue;
		}
		if ( strcmp( argv[i], "-V"  ) == 0 ) {
			if ( NULL == getnumnwathever ( &debufcount, argv[++i], sizeof(debufcount) ) ) {
				fprintf( stderr, "bad read argv\n" );
				exit(1);
			}
			continue;
		}
		if ( strcmp( argv[i], "-D"  ) == 0 ) {
			FLAGSET(mainwork,cdsm);
			continue;
		}
		if ( strcmp( argv[i], "-v"  ) == 0 ) {
			FLAGSET(mainwork,version);
			continue;
		}
		if ( strcmp( argv[i], "-a"  ) == 0 ) {
			FLAGSET(mainwork,mkasm);
			continue;
		}
		if ( strcmp( argv[i], "-s"  ) == 0 ) {
			FLAGSET(mainwork,casm);
			continue;
		}
		if ( strcmp( argv[i], "-m"  ) == 0 ) {
			if(NULL==fnmap) fnmap = malloc(256);
			readstr (fnmap, argv[++i], 256 );
			continue;
		}
		if ( strcmp( argv[i], "-cs"  ) == 0 ) {
			FLAGSET(mainwork,inflink);
			if(NULL==flink) flink = malloc(256);
			readstr (flink, argv[++i], 256 );
			continue;
		}

		break;
	}

	return i;
}

/* ---------------------------------------------------------- */
/*                                                            */
/* ---------------------------------------------------------- */

void showhelp ( void ) {

	fprintf( stderr, "Usage:\n" );
	fprintf( stderr, "pictc option asm_files\n" );
	
	fprintf( stderr, "Options:\n" );
	fprintf( stderr, "  -v              version\n" );
	fprintf( stderr, "  -a              analize asm files\n" );
	fprintf( stderr, "  -s              calculate asembler result\n" );
	fprintf( stderr, "  -d deasm_file   analize deasembler files\n" );
	fprintf( stderr, "  -D              calculate deasembler result\n" );
	fprintf( stderr, "  -m file         map file\n" );
	fprintf( stderr, "  -cs file        link file - info for bigest page ram\n" );
	fprintf( stderr, "  -V              verbose debug info - default 1 - still in work\n" );
	fprintf( stderr, "  -h              show this help\n" );
	fprintf( stderr, "  --help          show this help\n" );
}

/* ---------------------------------------------------------- */
/*            end file                                        */
/* ---------------------------------------------------------- */
